<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trans_docs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('costcenter_id')->nullable();
            $table->unsignedBigInteger('dir_bidang_id')->nullable();
            $table->unsignedBigInteger('currency_id')->default(1);
            $table->unsignedBigInteger('status_id');
            $table->date('date');
            $table->integer('tahun');
            $table->string('number', 64)->nullable();
            $table->boolean('is_holding')->default(TRUE);
            $table->string('aktifitas')->nullable();
            $table->string('desc_aktifitas')->nullable();
            $table->string('latar_belakang')->nullable();
            $table->double('ebitda')->nullable();
            $table->double('opex')->nullable();
            $table->string('dampak')->nullable();
            $table->string('resiko')->nullable();
            $table->text('doc_desc')->nullable();
            $table->boolean('is_kaitan_langsung')->nullable();
            $table->boolean('is_kaitan_tidak_langsung')->nullable();
            $table->text('point_perhatian_pak')->nullable();
            $table->double('dampak_pak')->nullable();
            $table->text('resiko_pak')->nullable();
            $table->text('rekomendasi_pak')->nullable();
            $table->text('dampak_atas')->nullable();
            $table->text('dampak_tidak_sesuai_ajuan')->nullable();
            $table->text('dampak_tidak_sesuai_realisasi')->nullable();
            $table->double('dampak_sesuai_rekomendasi')->nullable();
            $table->double('rekomendasi_tambah')->nullable();
            $table->unsignedBigInteger('user_atasan')->nullable();
            $table->unsignedBigInteger('user_atasan2')->nullable();
            $table->unsignedBigInteger('user_dir_bidang')->nullable();
            $table->unsignedBigInteger('user_dir_bidang_v2')->nullable();
            $table->unsignedBigInteger('user_dirkeu')->nullable();
            $table->json('files')->nullable();
            $table->json('input_data')->nullable();
            $table->boolean('active')->default(TRUE);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();

            $table->foreign('company_id')->references('id')
                ->on('companies')
                ->onUpdate('cascade');

            $table->foreign('costcenter_id')->references('id')
                ->on('cost_centers')
                ->onUpdate('cascade');

            $table->foreign('currency_id')->references('id')
                ->on('currencies')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trans_docs');
    }
};
