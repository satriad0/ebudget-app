<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('approval_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trans_doc_id');
            $table->string('action', 32)->comment('approve|revisi|reject|edit');
            $table->timestamp('action_at');
            $table->unsignedBigInteger('action_by');
            $table->unsignedBigInteger('doc_status');
            $table->unsignedBigInteger('prev_doc_status');
            $table->text('note')->nullable();
            $table->text('note_dirkeu')->nullable();
            $table->json('before')->nullable();
            $table->json('after')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('trans_doc_id')->references('id')
                ->on('trans_docs')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('approval_histories');
    }
};
