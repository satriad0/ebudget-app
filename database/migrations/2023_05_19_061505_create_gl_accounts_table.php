<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gl_accounts', function (Blueprint $table) {
            $table->id();
            $table->integer('gl_account');
            $table->text('description');
            $table->string('account_type');
            $table->string('cf_acc')->nullable();
            $table->string('cf_tb')->nullable();
            $table->string('contra_acc1')->nullable();
            $table->string('dimlist')->nullable();
            $table->string('fs_structure')->nullable();
            $table->string('group');
            $table->string('rate_type');
            $table->string('type_elim')->nullable();
            $table->integer('parenth1');
            $table->unsignedBigInteger('gl_id')->nullable();
            $table->dateTime('last_update')->nullable();
            $table->unsignedBigInteger('cash_flow');
            $table->unsignedBigInteger('group_cycle')->nullable();
            $table->unsignedBigInteger('structure_costing')->nullable();
            $table->unsignedBigInteger('group_hrgl_')->nullable();
            $table->integer('cluster_id')->nullable();
            $table->boolean('is_active')->default(TRUE);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gl_accounts');
    }
};
