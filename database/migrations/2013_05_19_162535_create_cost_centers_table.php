<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cost_centers', function (Blueprint $table) {
            $table->id();
            $table->string('cost_center_code');
            $table->text('description');
            $table->string('hirarchy')->nullable();
            $table->integer('cost_center_category')->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->string('profit_center')->nullable();
            $table->boolean('is_active')->default(TRUE);
            $table->integer('cost_center_category2')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('cost_category_desc')->nullable();
            $table->unsignedBigInteger('user_input_id')->nullable();
            $table->unsignedBigInteger('user_atasan_id')->nullable();
            $table->unsignedBigInteger('user_atasan2_id')->nullable();
            $table->unsignedBigInteger('user_dir_bidang_id')->nullable();
            $table->unsignedBigInteger('user_dirkeu_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();

            $table->foreign('company_id')->references('id')
                ->on('companies')
                ->onUpdate('cascade');

            $table->foreign('currency_id')->references('id')
                ->on('currencies')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cost_centers');
    }
};
