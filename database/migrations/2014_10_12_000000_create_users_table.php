<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->unique();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->string('no_badge')->nullable();
            $table->unsignedBigInteger('costcenter_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->boolean('is_user_sso')->default(TRUE);;
            $table->boolean('is_user_ldap')->default(TRUE);;

            $table->foreign('company_id')->references('id')
                ->on('companies')
                ->onUpdate('cascade');

            // $table->foreign('costcenter_id')->references('id')
            //     ->on('cost_centers')
            //     ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
