<?php

namespace Database\Seeders;

use App\Models\CostCenter;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class AdminUserRoleCaseSIG extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        $permissions = [
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'menu-list',
            'menu-create',
            'menu-edit',
            'menu-delete',
            'menu.item-list',
            'menu.item-create',
            'menu.item-edit',
            'menu.item-delete',
            'menu.costcenter',
            'menu.company',
            'menu.gljournal',
            'unfreeze-budget',
            'unfreeze-budget-pak',
            'approval',
            'submissionall',
            'history-approve'
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // create roles and assign existing permissions
        $roleSuperAdmin = Role::create(['name' => 'super-admin']);
         // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'Super Admin',
            'username' => 'superadmin',
            'email' => 'superadmin@example.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleSuperAdmin);

        $user = \App\Models\User::factory()->create([
            'name' => 'Admin Yopi',
            'username' => 'yopi.satria',
            'email' => 'yopi.satria@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleSuperAdmin);

        $user = \App\Models\User::factory()->create([
            'name' => 'Admin Joko Susilo',
            'username' => 'joko.susilo',
            'email' => 'joko.susilo@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleSuperAdmin);

        $role2 = Role::create(['name' => 'adminweb']);
        foreach ($permissions as $permission) {
            $role2->givePermissionTo($permission);
        }

        //User Submission
        $roleSubmission = Role::create(['name' => 'role-submission']);
        // $roleSubmission->givePermissionTo('permission list');
        // $roleSubmission->givePermissionTo('role list');
        // $roleSubmission->givePermissionTo('user list');
        // $roleSubmission->givePermissionTo('menu list');
        // $roleSubmission->givePermissionTo('menu.item list');
        $roleSubmission->givePermissionTo('unfreeze-budget');
        // $roleSubmission->givePermissionTo('menu.unfreze-budget-pak');
        $roleSubmission->givePermissionTo('approval');

        //Approval User
        $roleApprove = Role::create(['name' => 'role-approve']);
        $roleApprove->givePermissionTo('approval');
        $roleApprove->givePermissionTo('history-approve');

        // PAK
        $rolePak = Role::create(['name' => 'pak']);
        $rolePak->givePermissionTo('unfreeze-budget-pak');
        $rolePak->givePermissionTo('approval');
        
        // PAK
        $roleVpPak = Role::create(['name' => 'vp_pak']);
        $roleVpPak->givePermissionTo('approval');
    
        // gets all permissions via Gate::before rule; see AuthServiceProvider


    // ##################################################################################
       

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Yopi Satria SuperAdmin',
        //     'username'=> 'yopi.satria',
        //     'email' => 'yopi.satria@gmail.com',
        //     'company_id' => 1,
        //     'costcenter_id' => 1,
        // ]);
        // $user->assignRole($role3);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Joko Susilo Admin',
        //     'username' => 'joko.susilo',
        //     'email' => 'joko.susilo@gmail.com',
        //     'company_id' => 1,
        //     'costcenter_id' => 1,
        // ]);
        // $user->assignRole($role2);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Sanjay User',
        //     'username' => 'sanjay',
        //     'email' => 'sanjay@gmail.com',
        //     'company_id' => 1,
        //     'costcenter_id' => 1,
        // ]);
        // $user->assignRole($role1);

       

        $user = \App\Models\User::factory()->create([
            'name' => 'Admin PAK',
            'username'=> 'pak',
            'email' => 'pak@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($rolePak);

        
        $user = \App\Models\User::factory()->create([
            'name' => 'VP PAK',
            'username'=> 'vp_pak',
            'email' => 'vp_pak@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleVpPak);

        // user-user atasan
        $user = \App\Models\User::factory()->create([
            'name' => 'ADMIN DEP PROD EXCELLENCE',
            'username'=> 'admin_dep_prod',
            'email' => 'admin_dep_prod@gmail.com',
            'costcenter_id' => 179,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_input_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEP PROD EXCELLENCE',
            'username'=> 'dep_prod',
            'email' => 'dep_prod@gmail.com',
            'costcenter_id' => 178,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_atasan_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DIREKTORAT OPERASI',
            'username'=> 'dir_op',
            'email' => 'dir_op@gmail.com',
            'costcenter_id' => 176,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_dir_bidang_id' => $user->id]);
        $user->assignRole($role2);

        // dirkeu
        $user = \App\Models\User::factory()->create([
            'name' => 'ADMIN DEPARTEMEN AKUNTANSI',
            'username'=> 'admin_dep_akuntansi',
            'email' => 'admin_dep_akuntansi@gmail.com',
            'costcenter_id' => 57,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_input_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEPARTEMEN AKUNTANSI',
            'username'=> 'dep_akuntansi',
            'email' => 'dep_akuntansi@gmail.com',
            'costcenter_id' => 56,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_atasan_id' => $user->id]);
        $user->assignRole($role2);
        // ICT
        // SI202
        $user = \App\Models\User::factory()->create([
            'name' => 'ADMIN DEP OPERASI ICT',
            'username'=> 'admin_dep_op_ict',
            'email' => 'admin_dep_op_ict@gmail.com',
            'costcenter_id' => 86,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_input_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEP OPERASI ICT',
            'username'=> 'dep_op_ict',
            'email' => 'dep_op_ict@gmail.com',
            'costcenter_id' => 85,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_atasan_id' => $user->id]);
        $user->assignRole($role2);

        $ccDirkeu = CostCenter::where(['hirarchy' => 'SI2'])->first();
        $user = \App\Models\User::factory()->create([
            'name' => 'DIRKEU',
            'username'=> 'dirkeu',
            'email' => 'dirkeu@gmail.com',
            'costcenter_id' => $ccDirkeu->id,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_dir_bidang_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_dirkeu_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_dir_bidang_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_dirkeu_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_dirkeu_id' => $user->id]);
        $user->assignRole($role2);


        //input user
        foreach($this->userInput() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleSubmission);
        } 

        foreach($this->userAtasan1() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        } 
        foreach($this->userAtasan2() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        } 
        foreach($this->dirBidang() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        }
        
        foreach($this->dirKeu() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        } 
    }

    public function userInput()
    {
        //id,name,username,email,company_id,no_badge
        return [
            [1532,'FARIED MA\'RUF, SE.','faried.maruf','faried.maruf@sig.id',1,'1532',1],
            [1591,'MUHAMMAD FAKHRUDIN M., S.Kom.','muhammad.fakhrudin','muhammad.fakhrudin@sig.id',1,'1591',54],
            [1549,'ROIKHANATIN, SE.','roikhanatin','roikhanatin@sig.id',1,'1549',48],
            [1730,'HENDRAWAN TEGUH A., ST., MM.','hendrawan.teguh','hendrawan.teguh@sig.id',1,'1730',44],
            [1662,'RAHMAD ADIE PERDANA, ST.','rahmad.perdana','rahmad.perdana@sig.id',1,'1662',32],
            [6965,'RIAN DWI CAHYO, ST.','rian.cahyo','rian.cahyo@sig.id',1,'6965',221],
            [1502,'DENNI RIYANTO','denni.riyanto','denni.riyanto@sig.id',1,'1502',104],
            [1791,'NURINA PRATIWI SASMITA','nurina.pratiwi','nurina.pratiwi@sig.id',1,'1791',42],
            [868,'ABDUL MANAN, ST., MM.','abdul.manan','abdul.manan@sig.id',1,'868',50],
            [1741,'RAMA WIJAYA, ST.','rama.wijaya','rama.wijaya@sig.id',1,'1741',191],
            [6601,'ELISA MARIS HERU, SE., MM.','elisa.maris','elisa.maris@sig.id',1,'6601',53],
            [7007,'AWAN YOGATAMA, SE.','awan.yogatama','awan.yogatama@sig.id',1,'7007',132],
            [738,'NADYA KARNINA, S.M.','nadya.karnina','nadya.karnina@sig.id',1,'738',81],
            [1611,'AGUNG KURNIAWAN, SE.','agung.kurniawan','agung.kurniawan@sig.id',1,'1611',125],
            [7064,'MUHAMMAD SHIDQI, S.Ak.','muhammad.shidqi','muhammad.shidqi@sig.id',1,'7064',134],
            [791,'WAHYUNINGTIYAS','wahyuningtiyas','wahyuningtiyas@sig.id',1,'791',83],
            [6944,'UMI ZHAHRATUN NISA, MT.','umi.nisa','umi.nisa@sig.id',1,'6944',136],
            [3685,'SARI RAMADHANI, SE.','sari.ramadhani','sari.ramadhani@sig.id',1,'3685',293],
            [7014,'CHUSNUL ASFADILLAH, SE.','chusnul.asfadillah','chusnul.asfadillah@sig.id',1,'7014',175],
            [6980,'SUNGGING HARYO WICAKSONO, Ir., M.T., MSc','sungging.wicaksono','sungging.wicaksono@sig.id',1,'6980',218],
            [6995,'ARIFIANTO, ST.','arifianto','arifianto@sig.id',1,'6995',217],
            [7006,'ARIFUDDIN WAHYUDI, ST., MT.','arifuddin.wahyudi','arifuddin.wahyudi@sig.id',1,'7006',202],
            [3513,'RADITYA ALGADRI, ST.','raditya.algadri','raditya.algadri@sig.id',1,'3513',208],
            [706,'HEROE BOEDI LAKSONO','heroe.boedi','heroe.boedi@sig.id',1,'706',213],
            [6979,'ARY TRI WIBOWO, M.T., CSCM.','ary.wibowo','ary.wibowo@sig.id',1,'6979',302],
            [1224,'ANDRI SETIONO','andri.setiono1224','andri.setiono1224@sig.id',1,'1224',234],
            [1807,'RISQUL HIDAYAT, ST.','risqul.hidayat','risqul.hidayat@sig.id',1,'1807',240],
            [1803,'RISKA INAYATUL AINI','riska.aini','riska.aini@sig.id',1,'1803',260],
            [7735,'APEL RAMADHAN','apel.ranthy','apel.ranthy@sig.id',1,'7735',297],
            [6323,'CHANDRA GUSTAMA YUDHA','chandra.gustama','chandra.gustama@sig.id',1,'6323',283],
            [7050,'FINANTA PANJI RADIKA, S.Kom.','finanta.radika','finanta.radika@sig.id',1,'7050',243],
            [732,'FIRDIANSYAH OKTARIZKY, S.Sos.','firdiansyah.o','firdiansyah.o@sig.id',1,'732',296],
            [1762,'ARIS SODIK, ST.','aris.sodik','aris.sodik@sig.id',1,'1762',311],
            [6353,'ACHMAD AL MAHMUDI, ST.','achmad.mahmudi','achmad.mahmudi@sig.id',1,'6353',246],
            [6336,'JEREMIA MANGAPUL SILITONGA, ST.','jeremia.mangapul','jeremia.mangapul@sig.id',1,'6336',246],
            [6959,'WAHYU ADAM, S.Si.','wahyu.adam','wahyu.adam@sig.id',1,'6959',217],
            [7756,'HENRY KOESOEMO','henry.koesoemo','henry.koesoemo@sig.id',1,'7756',297],
            [3690,'EKO BAGUS PRIYUANTORO, ST.','eko.priyuantoro','eko.priyuantoro@sig.id',1,'3690',293],
            [1366,'I KETUT PANDE S., ST.','ketut.pande','ketut.pande@sig.id',1,'1366',234],
            [6354,'NAILU NUR IZZATI, ST.','nailu.nur','nailu.nur@sig.id',1,'6354',293],
            [6978,'IMRON GOZALI, ST., MT.','imron.gozali','imron.gozali@sig.id',1,'6978',302],
            [700,'HORAS SIBURIAN, SE.','horas.siburian','horas.siburian@sig.id',1,'700',214],
            [5327,'REDI GARJITO, S.Kom., MM.','redi.garjito','redi.garjito@sig.id',1,'5327',246],
            [1440,'ACHMAD CHOIRI, S.Si.','achmad.choiri','achmad.choiri@sig.id',1,'1440',246],
            [1713,'DIMAS DWI NOVARI, SH.','dimas.novari','dimas.novari@sig.id',1,'1713',246],
            [951,'SUPRIYADI, ST.','supriyadi.951','supriyadi.951@sig.id',1,'951',246],
            [6987,'MUHAMMAD FADHIL IMANSYAH, ST., MT.','mf.imansyah','mf.imansyah@sig.id',1,'6987',246],
            [1714,'BAYU PRATAMA PUTRA, S.Kom., M.T.','bayu.putra','bayu.putra@sig.id',1,'1714',246],
            [6964,'DEWI MELANI, M.Psi.','dewi.melani','dewi.melani@sig.id',1,'6964',246],
            [7004,'MUHAMMAD SALIM MARTAK, ST., MM.','muhammad.martak','muhammad.martak@sig.id',1,'7004',246],
        ];
        
    }
    public function userAtasan1()
    {
        return [
            [996,'EVIE AMALIANA, SE.','evie.amaliana','evie.amaliana@sig.id',1,'996',48],
            [8151,'ARIF GUNAWAN SULISTIYONO','arif.gunawan','arif.gunawan@sig.id',1,'8151',44],
            [737,'EDY SARAYA, ST., MBA., MM.','edy.saraya','edy.saraya@sig.id',1,'737',50],
            [7469,'PROID KONTURA MARTA','proid.kontura','proid.kontura@sig.id',1,'7469',221],
            [5259,'ADE MEIRIYADI, ST.','ade.meiriyadi','ade.meiriyadi@sig.id',1,'5259',173],
            [1786,'FEBRIANDITA KUSUMA, SE., MFin.','febriandita.kusuma','febriandita.kusuma@sig.id',1,'1786',42],
            [1066,'GUNTORO, SE.','guntoro.1066','guntoro.1066@sig.id',1,'1066',82],
            [1093,'AGUSWINARTO PUTRADJAKA, PIA.','aguswinarto.p','aguswinarto.p@sig.id',1,'1093',125],
            [7952,'FAJAR ANDHIKA','fajar.andhika','fajar.andhika@sig.id',1,'7952',134],
            [696,'WAHYU DARMAWAN, ST.','wahyu.darmawan','wahyu.darmawan@sig.id',1,'696',83],
            [5350,'ENDAH SULISTYO HARYANI, SE., Akt., MSi.','endah.haryani','endah.haryani@sig.id',1,'5350',136],
            [1689,'YUANITA PURNAMADEWI, SE., MBA.','yuanita.purnamadewi','yuanita.purnamadewi@sig.id',1,'1689',146],
            [7482,'RAHMAT FAIZAL','rahmat.faizal','rahmat.faizal@sig.id',1,'7482',293],
            [5237,'MAPPINCARA, S.Kom.','mappincara','mappincara@sig.id',1,'5237',165],
            [3642,'FAJAR ARISTYANTO, ST., MT.','fajar.aristyanto','fajar.aristyanto@sig.id',1,'3642',185],
            [1119,'OTTO ANDRI PRIYONO, ST., MM.','otto.priyono','otto.priyono@sig.id',1,'1119',217],
            [1778,'OKTORIA MASNIARI M.','oktoria.masniari','oktoria.masniari@sig.id',1,'1778',42],
            [4536,'AGUS SUBROTO, Ir.','agus.subroto','agus.subroto@sig.id',1,'4536',208],
            [4530,'SARIATUN, Ir., MM.','sariatun','sariatun@sig.id',1,'4530',214],
            [2990,'AMRAL AHMAD, ST.','amral.ahmad','amral.ahmad@sig.id',1,'2990',240],
            [3038,'AHMAD ARIS, ST.','ahmad.aris','ahmad.aris@sig.id',1,'3038',240],
            [4591,'AGUS SANYOTO, Ir., MM.','agus.sanyoto','agus.sanyoto@sig.id',1,'4591',208],
            [1697,'MUHAMMAD TAQIYUDDIN F., S.Kom.','m.taqiyuddin','m.taqiyuddin@sig.id',1,'1697',146],
            [4567,'AHMAD ZULKARNAIN, Ir.','ahmad.zulkarnain','ahmad.zulkarnain@sig.id',1,'4567',283],
            [7717,'IRMA OKTARINI','irma.oktarini','irma.oktarini@sig.id',1,'7717',260],
            [7741,'DEDE KUSNAWAN','dede.kusnawan','dede.kusnawan@sig.id',1,'7741',243],
            [6297,'YOSEPH BUDI WICAKSONO, ST., MSc.','yoseph.budi','yoseph.budi@sig.id',1,'6297',302],
            [8098,'JUHANS SURYANTAN','juhans.suryantan','juhans.suryantan@sig.id',1,'8098',296],
            [701,'ACHMAD NURIL, S.Pd.','achmad.nuril','achmad.nuril@sig.id',1,'701',311],
            [5305,'MUH.AFANDY MARASABESSY, SE.','muh.marasabessy','muh.marasabessy@sig.id',1,'5305',136],
            [7873,'IRFAN ARIEF','irfan.arief','irfan.arief@sig.id',1,'7873',260],
            [7877,'HERMAN RINALDI','herman.rinaldi','herman.rinaldi@sig.id',1,'7877',134],
            [7857,'EKI KARMANTO','eki.karmanto','eki.karmanto@sig.id',1,'7857',221],
            [1457,'MOHAMMAD FAISAL STANY, SH., MM.','mohammad.stany','mohammad.stany@sig.id',1,'1457',42],
            [7780,'RIZKIE AGUSTIANSYAH','rizkie.agustiansyah','rizkie.agustiansyah@sig.id',1,'7780',260],
            [3691,'HANNY K. LUKITO, ST.','hanny.lukito','hanny.lukito@sig.id',1,'3691',1],
            [7752,'SASHA MEDIA','sasha.media','sasha.media@sig.id',1,'7752',1],
            [6429,'ADEL RAHADI RASYID, ST., MCom.','adel.rahadi','adel.rahadi@sig.id',1,'6429',1],
            [3701,'ROMI ABDILAH, Ir.','romi.abdilah','romi.abdilah@sig.id',1,'3701',1],
            [4901,'SULAIHA MUHYIDDIN, SE, M.BUS, Ak','sulaiha.muhyiddin','sulaiha.muhyiddin@sig.id',1,'4901',1],
            [1092,'HER ARSA PAMBUDI, SE., M.MT.','her.pambudi','her.pambudi@sig.id',1,'1092',1],
            [6290,'DEDDY IRFANDY, S.Kom., SE., MBA., Ak.','deddy.irfandy','deddy.irfandy@sig.id',1,'6290',1],
            [807,'IRMAWATI SRI A., Ir., MT.','irmawati.sri','irmawati.sri@sig.id',1,'807',1],
            [6350,'CHANDRA PRADIPTA, SH.','chandra.pradipta','chandra.pradipta@sig.id',1,'6350',1],
            [1763,'HALIM ALFATAH, S.Psi., MM.','halim.alfatah','halim.alfatah@sig.id',1,'1763',1],
            [3576,'MUHAMAD IKRAR, ST., MM.','muhamad.ikrar','muhamad.ikrar@sig.id',1,'3576',1],
            [1049,'M. LUDFI SETYADI, Ir.','ludfi.setyadi','ludfi.setyadi@sig.id',1,'1049',1],
            [8006,'IRNA RAHMALIA','irna.rahmalia','irna.rahmalia@sig.id',1,'8006',1],
            [1705,'VITRIA DANIATI, ST.','vitria.daniati','vitria.daniati@sig.id',1,'1705',1],
        ];
    }
    public function userAtasan2()
    {
        return [
            [7475,'VITA MAHREYNI','vita.mahreyni','vita.mahreyni@sig.id',1,'7475',15],
            [7943,'PRAMONO HARJANTO','pramono.harjanto','pramono.harjanto@sig.id',1,'7943',2],
            [7445,'HADI SETIADI','hadi.setiadi','hadi.setiadi@sig.id',1,'7445',35],
            [8065,'ANINDIO DANESWARA','anindio.daneswara','anindio.daneswara@sig.id',1,'8065',37],
            [6332,'NOVA KURNIAWAN, SE.','nova.kurniawan','nova.kurniawan@sig.id',1,'6332',42],
            [7651,'JOHANNA DAUNAN','johanna.dauna','johanna.dauna@semenindonesia.com',1,'7651',51],
            [7663,'ANTONIUS ARDIAN BERMANA','antonius.bermana','antonius.bermana@sig.id',1,'7663',53],
            [1098,'HASAN ARIFIN, SE.','hasan.arifin','hasan.arifin@sig.id',1,'1098',53],
            [1270,'JONI GUNAWAN, S.Sos., MM.','joni.gunawan','joni.gunawan@sig.id',1,'1270',285],
            [7652,'PEGGY FATHIYA','peggy.fathiya','peggy.fathiya@sig.id',1,'7652',174],
            [2978,'BENNY ISMANTO, Ir.','benny.ismanto','benny.ismanto@sig.id',1,'2978',177],
            [1646,'HERU INDRAWIDJAJANTO, Ir., M.MT.','heru.indrawidjajanto','heru.indrawidjajanto@sig.id',1,'1646',234],
            [7942,'ANDI KRISHNA ARINALDI','andi.krishna','andi.krishna@sig.id',1,'7942',294],
            [1548,'AKHMAD YANI YULIANTO, SE., MT., CSCM','akhmad.yulianto','akhmad.yulianto@sig.id',1,'1548',261],
            [7287,'RAHMAN KURNIAWAN','rahman.kurniawan','rahman.kurniawan@sig.id',1,'7287',2],
            [8128,'MARALDA HERNANDA KAIRUPAN','maralda.kairupan','maralda.kairupan@sig.id',1,'8128',2],
            [8130,'SUMARLAN WIBAWA','sumarlan.wibawa','sumarlan.wibawa@sig.id',1,'8130',2],
        ];

       

    }
    public function dirBidang()
    {
        return [
            [8101,'DONNY ARSAL','DONNY.ARSAL','DONNY.ARSAL@sig.id',1,'8101',2],
            [8077,'YOSVIANDRI','yosviandri','yosviandri@sig.id',1,'8077',147],
            [8187,'SUBHAN, SE. AK. MM. CA','SUBHAN','SUBHAN@sig.id',1,'8187',242],
            [8188,'RENI WULANDARI','reni.wulandari','reni.wulandari@sig.id',1,'8188',176],
            [8083,'AGUNG WIHARTO, S.IP.','agung.wiharto','agung.wiharto@sig.id',1,'8083',2],
        ];
    }
    public function dirKeu()
    {
        //id,name,username,email,company_id,no_badge
        return [
            [8093,'ANDRIANO HOSNY PANANGIAN','HOSNY.PANANGIAN','HOSNY.PANANGIAN@sig.id',1,'8093',55],
        ];
    }
}
