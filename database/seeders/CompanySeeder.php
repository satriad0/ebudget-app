<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    
    public function run(): void
    {
        foreach($this->data() as $row){
            Company::create([
                'company_code' => $row[0],
                'company_name' => $row[1],
                'address' => $row[2],
                'country' => $row[3],
                'currency_id' => $row[4],
                'hirarchy' => $row[5],
                'fm_area' => $row[6],
                'type' => $row[7],                
                'is_active' => TRUE,
                'start_date' => '2021-01-01',
                'end_date' => '9999-12-30',
                'created_by' => 1,
                'updated_by' => 0,
            ]);
        }    

        // Company::create([
        //     'company_code' => '1000',
        //     'company_name' => 'Holding SGG',
        //     'address' => 'Jl. Kuningan Mulia Kav. 9, Kawasan Bisnis Epicentrum, Jakarta Selatan - Indonesia 12980',
        //     'country' => 'ID',
        //     'currency_id' => 1,
        //     'hirarchy' => null,
        //     'FM_Area' => 'SGG1',
        //     'is_active' => TRUE,
        //     'start_date' => '2021-10-10',
        //     'end_date' => '2026-10-10',
        //     'created_by' => 1,
        //     'updated_by' => 0,
        // ],[
        //     'company_code' => '2000',
        //     'company_name' => 'PT. Semen Indonesia',
        //     'address' => 'Jl. Jakarta Selatan - Indonesia',
        //     'country' => 'ID',
        //     'currency_id' => 1,
        //     'hirarchy' => '1000',
        //     'FM_Area' => 'SGG2',
        //     'is_active' => TRUE,
        //     'start_date' => '2021-10-10',
        //     'end_date' => '2026-10-10',
        //     'created_by' => 1,
        //     'updated_by' => 0,
        // ]);
    }

    public function data()
    {
        return [
            ['1000','Holding SGG','Jakarta','ID',1,null,'SGG1','Holding'],
            ['2000','PT. Semen Indonesia','Gresik','ID',1,'1000','SGG2','Holding'],
            ['2100','Head Office Jakarta','Jakarta','ID',1,'1000','SGHO','Holding'],
            ['3000','PT. Semen Padang','Padang','ID',1,'1000','SGG4','AP Semen'],
            ['4000','PT. Semen Tonasa','Tonasa','ID',1,'1000','SGG5','AP Semen'],
            ['5000','PT. Semen Gresik','Gresik','ID',1,'1000','SGG6','AP Semen'],
            ['6000','TLCC','Hanoi','VN',2,'1000','SGG6','AP Semen'],
            ['7000','PT. Semen Indonesia','Gresik','ID',1,'1000','SGG7','AP Semen'],
            ['7900','PT. Semen Indonesia','Gresik','ID',1,'1000','SGMD','AP Semen'],
            
            ['7KSO','PT KSO Operation SI','Gresik','ID',1,'1000','SGGK','AP Semen'],
            ['8000','PT Semen Indonesia Aceh','Gresik','ID',1,'1000','SGG8','AP Semen'],
            ['9000','PT Semen Kupang Indonesia','Kupang','ID',1,'1000','SGG9','AP Semen'],
            ['G100','PT SGG Energi Prima','Gresik','ID',1,'1000','G100','AP Semen'],
            ['G200','PT SEMEN INDONESIA BETON','Jakarta','ID',1,'1000','G200','AP Semen'],

            ['G210','Varia Usaha Beton','Sidoarjo','ID',1,'1000',null,'AP Non Semen'],
            ['G300','PT. SISI','Jakarta','ID',1,'1000','G300','AP Non Semen'],
            ['G400','SIIB','Jakarta','ID',1,'1000','G400','AP Non Semen'],
            ['G600','Kawasan Industri Gresik','Gresik','ID',1,'1000',null,'AP Non Semen'],
        ];
        
    }
}
