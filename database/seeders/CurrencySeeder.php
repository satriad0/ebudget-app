<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $currencies = [
                [
                    'id'  => 1,
                    'code'  => 'IDR',
                    'currency_name' => 'Rupiah',
                    'prefix_symbol' => 'Rp',
                    'suffix_symbol' => ',-',
                    'created_by' => 1,
                    'updated_by' => 0,
                ],
                [
                    'id'  => 2,
                    'code' => 'VND',
                    'currency_name' => 'đồng Vietnam',
                    'prefix_symbol' => null,
                    'suffix_symbol' => '₫',
                    'created_by' => 1,
                    'updated_by' => 0,
                ],[
                    'id'  => 3,
                    'code' => 'USD',
                    'currency_name' => 'Dollar',
                    'prefix_symbol' => '$',
                    'suffix_symbol' => null,
                    'created_by' => 1,
                    'updated_by' => 0,
                ]
            ];

        foreach ($currencies as $currency) {
            Currency::create($currency);
        }
    }
}
