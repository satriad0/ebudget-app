<?php

namespace Database\Seeders;

use App\Models\CostCenter;
use Illuminate\Database\Seeder;
use BalajiDharma\LaravelMenu\Models\Menu;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class AdminCoreSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        $permissions = [
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'menu-list',
            'menu-create',
            'menu-edit',
            'menu-delete',
            'menu.item-list',
            'menu.item-create',
            'menu.item-edit',
            'menu.item-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'writer']);
        $role1->givePermissionTo('permission-list');
        $role1->givePermissionTo('role-list');
        $role1->givePermissionTo('user-list');
        $role1->givePermissionTo('menu-list');
        $role1->givePermissionTo('menu.item-list');

        $role2 = Role::create(['name' => 'admin']);
        foreach ($permissions as $permission) {
            $role2->givePermissionTo($permission);
        }

        $role3 = Role::create(['name' => 'super-admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'Super Admin',
            'username' => 'superadmin',
            'email' => 'superadmin@example.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($role3);

        $user = \App\Models\User::factory()->create([
            'name' => 'Yopi Satria SuperAdmin',
            'username'=> 'yopi.satria',
            'email' => 'yopi.satria@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($role3);

        $user = \App\Models\User::factory()->create([
            'name' => 'Joko Susilo Admin',
            'username' => 'joko.susilo',
            'email' => 'joko.susilo@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($role2);

        $user = \App\Models\User::factory()->create([
            'name' => 'Sanjay User',
            'username' => 'sanjay',
            'email' => 'sanjay@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($role1);

        // PAK
        $rolePak = Role::create(['name' => 'pak']);
        $user = \App\Models\User::factory()->create([
            'name' => 'Admin PAK',
            'username'=> 'pak',
            'email' => 'pak@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($rolePak);
        $roleVpPak = Role::create(['name' => 'vp_pak']);
        $user = \App\Models\User::factory()->create([
            'name' => 'VP PAK',
            'username'=> 'vp_pak',
            'email' => 'vp_pak@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleVpPak);

        // user-user atasan
        $user = \App\Models\User::factory()->create([
            'name' => 'ADMIN DEP PROD EXCELLENCE',
            'username'=> 'admin_dep_prod',
            'email' => 'admin_dep_prod@gmail.com',
            'costcenter_id' => 179,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_input_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEP PROD EXCELLENCE',
            'username'=> 'dep_prod',
            'email' => 'dep_prod@gmail.com',
            'costcenter_id' => 178,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_atasan_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DIREKTORAT OPERASI',
            'username'=> 'dir_op',
            'email' => 'dir_op@gmail.com',
            'costcenter_id' => 176,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_dir_bidang_id' => $user->id]);
        $user->assignRole($role2);

        // dirkeu
        $user = \App\Models\User::factory()->create([
            'name' => 'ADMIN DEPARTEMEN AKUNTANSI',
            'username'=> 'admin_dep_akuntansi',
            'email' => 'admin_dep_akuntansi@gmail.com',
            'costcenter_id' => 57,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_input_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEPARTEMEN AKUNTANSI',
            'username'=> 'dep_akuntansi',
            'email' => 'dep_akuntansi@gmail.com',
            'costcenter_id' => 56,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_atasan_id' => $user->id]);
        $user->assignRole($role2);
        // ICT
        // SI202
        $user = \App\Models\User::factory()->create([
            'name' => 'ADMIN DEP OPERASI ICT',
            'username'=> 'admin_dep_op_ict',
            'email' => 'admin_dep_op_ict@gmail.com',
            'costcenter_id' => 86,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_input_id' => $user->id]);
        $user->assignRole($role2);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEP OPERASI ICT',
            'username'=> 'dep_op_ict',
            'email' => 'dep_op_ict@gmail.com',
            'costcenter_id' => 85,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_atasan_id' => $user->id]);
        $user->assignRole($role2);

        $ccDirkeu = CostCenter::where(['hirarchy' => 'SI2'])->first();
        $user = \App\Models\User::factory()->create([
            'name' => 'DIRKEU',
            'username'=> 'dirkeu',
            'email' => 'dirkeu@gmail.com',
            'costcenter_id' => $ccDirkeu->id,
            'company_id' => 1,
        ]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_dir_bidang_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI201_')->update(['user_dirkeu_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_dir_bidang_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI202_')->update(['user_dirkeu_id' => $user->id]);
        CostCenter::where('hirarchy', 'LIKE', 'SI301_')->update(['user_dirkeu_id' => $user->id]);
        $user->assignRole($role2);

        // create menu
        $menu = Menu::create([
            'name' => 'Admin',
            'machine_name' => 'admin',
            'description' => 'Admin Menu',
        ]);        

        $menu_items = [
            [
                'name'      => 'Dashboard',
                'uri'       => '/dashboard',
                'enabled'   => 1,
                'weight'    => 0,
                'icon'      => "M13,3V9H21V3M13,21H21V11H13M3,21H11V15H3M3,13H11V3H3V13Z"
            ],
            [
                'name'      => 'Submision All',
                'uri'       => '/submissionall',
                'enabled'   => 1,
                'weight'    => 1,
                'icon'      => "M13.5,8H12V13L16.28,15.54L17,14.33L13.5,12.25V8M13,3A9,9 0 0,0 4,12H1L4.96,16.03L9,12H6A7,7 0 0,1 13,5A7,7 0 0,1 20,12A7,7 0 0,1 13,19C11.07,19 9.32,18.21 8.06,16.94L6.64,18.36C8.27,20 10.5,21 13,21A9,9 0 0,0 22,12A9,9 0 0,0 13,3"
            ],
            [
                'name'      => 'Unfreeze Budget',
                'uri'       => '/unfreeze-budget',
                'enabled'   => 1,
                'weight'    => 1,
                'icon'      => "M13.5,8H12V13L16.28,15.54L17,14.33L13.5,12.25V8M13,3A9,9 0 0,0 4,12H1L4.96,16.03L9,12H6A7,7 0 0,1 13,5A7,7 0 0,1 20,12A7,7 0 0,1 13,19C11.07,19 9.32,18.21 8.06,16.94L6.64,18.36C8.27,20 10.5,21 13,21A9,9 0 0,0 22,12A9,9 0 0,0 13,3"
            ],
            [
                'name'      => 'Unfreeze Budget AP',
                'uri'       => '/unfreeze-budget-ap',
                'enabled'   => 1,
                'weight'    => 1,
                'icon'      => "M13.5,8H12V13L16.28,15.54L17,14.33L13.5,12.25V8M13,3A9,9 0 0,0 4,12H1L4.96,16.03L9,12H6A7,7 0 0,1 13,5A7,7 0 0,1 20,12A7,7 0 0,1 13,19C11.07,19 9.32,18.21 8.06,16.94L6.64,18.36C8.27,20 10.5,21 13,21A9,9 0 0,0 22,12A9,9 0 0,0 13,3"
            ],
            [
                'name'      => 'Submission',
                'uri'       => '/submission',
                'enabled'   => 1,
                'weight'    => 1,
                'icon'      => 'M13 19C13 20.1 13.3 21.12 13.81 22H6C4.89 22 4 21.11 4 20V4C4 2.9 4.89 2 6 2H7V9L9.5 7.5L12 9V2H18C19.1 2 20 2.89 20 4V13.09C19.67 13.04 19.34 13 19 13C15.69 13 13 15.69 13 19M20 18V15H18V18H15V20H18V23H20V20H23V18H20Z',
            ],
            [
                'name'      => 'Approval',
                'uri'       => '/approval',
                'enabled'   => 1,
                'weight'    => 2,
                'icon'      => 'M12 2C6.5 2 2 6.5 2 12S6.5 22 12 22 22 17.5 22 12 17.5 2 12 2M10 17L5 12L6.41 10.59L10 14.17L17.59 6.58L19 8L10 17Z',
            ],
            // [
            //     'name'      => 'Unfreeze',
            //     'uri'       => '/unfreeze',
            //     'enabled'   => 1,
            //     'weight'    => 2,
            //     'icon'      => 'M16.46,9.41L13,7.38V5.12L14.71,3.41L13.29,2L12,3.29L10.71,2L9.29,3.41L11,5.12V7.38L8.5,8.82L6.5,7.69L5.92,5.36L4,5.88L4.47,7.65L2.7,8.12L3.22,10.05L5.55,9.43L7.55,10.56V13.45L5.55,14.58L3.22,13.96L2.7,15.89L4.47,16.36L4,18.12L5.93,18.64L6.55,16.31L8.55,15.18L11,16.62V18.88L9.29,20.59L10.71,22L12,20.71L13.29,22L14.7,20.59L13,18.88V16.62L16.46,14.61M9.5,10.56L12,9.11L14.5,10.56V13.44L12,14.89L9.5,13.44M19,13V7H21V13H19M19,17V15H21V17H19Z',
            // ],
            [
                'name'      => 'History Approve',
                'uri'       => '/historyapprove',
                'enabled'   => 1,
                'weight'    => 1,
                'icon'      => "M13.5,8H12V13L16.28,15.54L17,14.33L13.5,12.25V8M13,3A9,9 0 0,0 4,12H1L4.96,16.03L9,12H6A7,7 0 0,1 13,5A7,7 0 0,1 20,12A7,7 0 0,1 13,19C11.07,19 9.32,18.21 8.06,16.94L6.64,18.36C8.27,20 10.5,21 13,21A9,9 0 0,0 22,12A9,9 0 0,0 13,3"
            ],
            [
                'name'      => 'Master Cost Center',
                'uri'       => '/mastercost',
                'enabled'   => 1,
                'weight'    => 2,
                'icon'      => 'M3,6H21V18H3V6M12,9A3,3 0 0,1 15,12A3,3 0 0,1 12,15A3,3 0 0,1 9,12A3,3 0 0,1 12,9M7,8A2,2 0 0,1 5,10V14A2,2 0 0,1 7,16H17A2,2 0 0,1 19,14V10A2,2 0 0,1 17,8H7Z',
            ],
            [
                'name'      => 'Master GI Journal',
                'uri'       => '/mastergl',
                'enabled'   => 1,
                'weight'    => 2,
                'icon'      => 'M3,7V5H5V4C5,2.89 5.9,2 7,2H13V9L15.5,7.5L18,9V2H19C20.05,2 21,2.95 21,4V20C21,21.05 20.05,22 19,22H7C5.95,22 5,21.05 5,20V19H3V17H5V13H3V11H5V7H3M7,11H5V13H7V11M7,7V5H5V7H7M7,19V17H5V19H7Z',
            ],
            [
                'name'      => 'Master Company',
                'uri'       => '/mastercomp',
                'enabled'   => 1,
                'weight'    => 2,
                'icon'      => 'M18,15H16V17H18M18,11H16V13H18M20,19H12V17H14V15H12V13H14V11H12V9H20M10,7H8V5H10M10,11H8V9H10M10,15H8V13H10M10,19H8V17H10M6,7H4V5H6M6,11H4V9H6M6,15H4V13H6M6,19H4V17H6M12,7V3H2V21H22V7H12Z',
            ],
            [
                'name'      => 'Permissions',
                'uri'       => '/<admin>/permission',
                'enabled'   => 1,
                'weight'    => 3,
                'icon'      => 'M11 10V12H9V14H7V12H5.8C5.4 13.2 4.3 14 3 14C1.3 14 0 12.7 0 11S1.3 8 3 8C4.3 8 5.4 8.8 5.8 10H11M3 10C2.4 10 2 10.4 2 11S2.4 12 3 12 4 11.6 4 11 3.6 10 3 10M16 14C18.7 14 24 15.3 24 18V20H8V18C8 15.3 13.3 14 16 14M16 12C13.8 12 12 10.2 12 8S13.8 4 16 4 20 5.8 20 8 18.2 12 16 12Z',
            ],
            [
                'name'      => 'Roles',
                'uri'       => '/<admin>/role',
                'enabled'   => 1,
                'weight'    => 4,
                'icon'      => 'M15.5,12C18,12 20,14 20,16.5C20,17.38 19.75,18.21 19.31,18.9L22.39,22L21,23.39L17.88,20.32C17.19,20.75 16.37,21 15.5,21C13,21 11,19 11,16.5C11,14 13,12 15.5,12M15.5,14A2.5,2.5 0 0,0 13,16.5A2.5,2.5 0 0,0 15.5,19A2.5,2.5 0 0,0 18,16.5A2.5,2.5 0 0,0 15.5,14M10,4A4,4 0 0,1 14,8C14,8.91 13.69,9.75 13.18,10.43C12.32,10.75 11.55,11.26 10.91,11.9L10,12A4,4 0 0,1 6,8A4,4 0 0,1 10,4M2,20V18C2,15.88 5.31,14.14 9.5,14C9.18,14.78 9,15.62 9,16.5C9,17.79 9.38,19 10,20H2Z',
            ],
            [
                'name'      => 'Users',
                'uri'       => '/<admin>/user',
                'enabled'   => 1,
                'weight'    => 5,
                'icon'      => 'M12,5.5A3.5,3.5 0 0,1 15.5,9A3.5,3.5 0 0,1 12,12.5A3.5,3.5 0 0,1 8.5,9A3.5,3.5 0 0,1 12,5.5M5,8C5.56,8 6.08,8.15 6.53,8.42C6.38,9.85 6.8,11.27 7.66,12.38C7.16,13.34 6.16,14 5,14A3,3 0 0,1 2,11A3,3 0 0,1 5,8M19,8A3,3 0 0,1 22,11A3,3 0 0,1 19,14C17.84,14 16.84,13.34 16.34,12.38C17.2,11.27 17.62,9.85 17.47,8.42C17.92,8.15 18.44,8 19,8M5.5,18.25C5.5,16.18 8.41,14.5 12,14.5C15.59,14.5 18.5,16.18 18.5,18.25V20H5.5V18.25M0,20V18.5C0,17.11 1.89,15.94 4.45,15.6C3.86,16.28 3.5,17.22 3.5,18.25V20H0M24,20H20.5V18.25C20.5,17.22 20.14,16.28 19.55,15.6C22.11,15.94 24,17.11 24,18.5V20Z',
            ],
            [
                'name'      => 'Menus',
                'uri'       => '/<admin>/menu',
                'enabled'   => 1,
                'weight'    => 6,
                'icon'      => 'M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z',
            ],

        ];

        $menu->menuItems()->createMany($menu_items);
    }
}
