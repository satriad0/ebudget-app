<?php

namespace Database\Seeders;

use App\Models\CostCenter;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class AdminUserRoleCaseSIG extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        $permissions = [
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'menu-list',
            'menu-create',
            'menu-edit',
            'menu-delete',
            'menu.item-list',
            'menu.item-create',
            'menu.item-edit',
            'menu.item-delete',
            'menu.costcenter',
            'menu.company',
            'menu.gljournal',
            'unfreeze-budget',
            'unfreeze-budget-pak',
            'approval',
            'submissionall',
            'history-approve'
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // create roles and assign existing permissions
        $roleSuperAdmin = Role::create(['name' => 'super-admin']);
         // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'Super Admin',
            'username' => 'superadmin',
            'email' => 'superadmin@example.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleSuperAdmin);

        $user = \App\Models\User::factory()->create([
            'name' => 'Admin Yopi',
            'username' => 'yopi.satria',
            'email' => 'yopi.satria@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleSuperAdmin);

        $user = \App\Models\User::factory()->create([
            'name' => 'Admin Joko Susilo',
            'username' => 'joko.susilo',
            'email' => 'joko.susilo@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleSuperAdmin);

        $role2 = Role::create(['name' => 'adminweb']);
        foreach ($permissions as $permission) {
            $role2->givePermissionTo($permission);
        }

        //User Submission
        $roleSubmission = Role::create(['name' => 'role-submission']);
        // $roleSubmission->givePermissionTo('permission list');
        // $roleSubmission->givePermissionTo('role list');
        // $roleSubmission->givePermissionTo('user list');
        // $roleSubmission->givePermissionTo('menu list');
        // $roleSubmission->givePermissionTo('menu.item list');
        $roleSubmission->givePermissionTo('unfreeze-budget');
        // $roleSubmission->givePermissionTo('menu.unfreze-budget-pak');
        $roleSubmission->givePermissionTo('approval');

        //Approval User
        $roleApprove = Role::create(['name' => 'role-approve']);
        $roleApprove->givePermissionTo('approval');
        $roleApprove->givePermissionTo('history-approve');

        // PAK
        $rolePak = Role::create(['name' => 'pak']);
        $rolePak->givePermissionTo('unfreeze-budget-pak');
        $rolePak->givePermissionTo('approval');
        
        // PAK
        $roleVpPak = Role::create(['name' => 'vp_pak']);
        $roleVpPak->givePermissionTo('approval');
    
        // gets all permissions via Gate::before rule; see AuthServiceProvider


    // ##################################################################################
       

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Yopi Satria SuperAdmin',
        //     'username'=> 'yopi.satria',
        //     'email' => 'yopi.satria@gmail.com',
        //     'company_id' => 1,
        //     'costcenter_id' => 1,
        // ]);
        // $user->assignRole($role3);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Joko Susilo Admin',
        //     'username' => 'joko.susilo',
        //     'email' => 'joko.susilo@gmail.com',
        //     'company_id' => 1,
        //     'costcenter_id' => 1,
        // ]);
        // $user->assignRole($role2);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Sanjay User',
        //     'username' => 'sanjay',
        //     'email' => 'sanjay@gmail.com',
        //     'company_id' => 1,
        //     'costcenter_id' => 1,
        // ]);
        // $user->assignRole($role1);

       

        $user = \App\Models\User::factory()->create([
            'name' => 'Admin PAK',
            'username'=> 'pak',
            'email' => 'pak@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($rolePak);

        
        $user = \App\Models\User::factory()->create([
            'name' => 'VP PAK',
            'username'=> 'vp_pak',
            'email' => 'vp_pak@gmail.com',
            'company_id' => 1,
            'costcenter_id' => 1,
        ]);
        $user->assignRole($roleVpPak);


        //input user
        foreach($this->userInput() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleSubmission);
        } 

        foreach($this->userAtasan1() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        } 
        foreach($this->userAtasan2() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        } 
        foreach($this->dirBidang() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        }
        
        foreach($this->dirKeu() as $row){
            $user = \App\Models\User::factory()->create([
                'id' => $row[0],
                'name' => $row[1],
                'username'=> $row[2],
                'email' => $row[3],
                'company_id' => $row[4],
                'no_badge' => $row[5],
                'costcenter_id' => $row[6],
            ]);            
            $user->assignRole($roleApprove);
        } 
    }

    public function userInput()
    {
        //id,name,username,email,company_id,no_badge
        return [
            [706,'HEROE BOEDI LAKSONO','heroe.boedi','heroe.boedi@sig.id',1,'706',null],
            [721,'AHMAD PARNO SAVERILLAH, SE., M.I.Kom','parno.721','parno.721@sig.id',1,'721',null],
            [732,'FIRDIANSYAH OKTARIZKY, S.Sos.','firdiansyah.o','firdiansyah.o@sig.id',1,'732',null],
            [738,'NADYA KARNINA, S.M.','nadya.karnina','nadya.karnina@sig.id',1,'738',null],
            [791,'WAHYUNINGTIYAS','wahyuningtiyas','wahyuningtiyas@sig.id',1,'791',null],
            // [807,'IRMAWATI SRI A., Ir., MT.','irmawati.sri','irmawati.sri@sig.id',1,'807',null],
            [809,'BAGUS DWIWASONO, ST., CPSLog.','bagus.dwiwasono','bagus.dwiwasono@sig.id',1,'809',null],
            [818,'SLAMET MURSIDIARSO, ST.','slamet.mursidiarso','slamet.mursidiarso@sig.id',1,'818',null],
            [868,'ABDUL MANAN, ST., MM.','abdul.manan','abdul.manan@sig.id',1,'868',null],
            [923,'AKHMAD BASUNI, SE.','akhmad.basuni','akhmad.basuni@sig.id',1,'923',null],
            [924,'UMMI CHOLSUM','ummi.cholsum','ummi.cholsum@sig.id',1,'924',null],
            [951,'SUPRIYADI, ST.','supriyadi.951','supriyadi.951@sig.id',1,'951',null],
            [1023,'SHINTA WINDU H.P., SE., PIA.','shinta.windu','shinta.windu@sig.id',1,'1023',null],
            [1058,'DWI PURWANTO, SE.','dwi.purwanto','dwi.purwanto@sig.id',1,'1058',null],
            [1138,'ARIF SANTOSO','arif.santoso1138','arif.santoso1138@sig.id',1,'1138',null],
            [1152,'BUDI SETYO NUGROHO., S.Kom.','budi.setyo','budi.setyo@sig.id',1,'1152',null],
            [1172,'HADI SISWANTO, S.M.','hadi.siswanto','hadi.siswanto@sig.id',1,'1172',null],
            [1211,'TAUFAN MAULANA, ST.','taufan.maulana','taufan.maulana@sig.id',1,'1211',null],
            [1224,'ANDRI SETIONO','andri.setiono1224','andri.setiono1224@sig.id',1,'1224',null],
            [1328,'SIGIT SUHARTONO, SE., Akt.','sigit.suhartono','sigit.suhartono@sig.id',1,'1328',null],
            [1366,'I KETUT PANDE S., ST.','ketut.pande','ketut.pande@sig.id',1,'1366',null],
            [1386,'HARI SUPRAPTO, SH.','hari.suprapto','hari.suprapto@sig.id',1,'1386',null],
            [1440,'ACHMAD CHOIRI, S.Si.','achmad.choiri','achmad.choiri@sig.id',1,'1440',null],
            [1502,'DENNI RIYANTO','denni.riyanto','denni.riyanto@sig.id',1,'1502',null],
            [1532,'FARIED MA\'RUF, SE.','faried.maruf','faried.maruf@sig.id',1,'1532',null],
            [1552,'MOH. MUHLIS ZAINUDDIN, ST.','muhlis.zainuddin','muhlis.zainuddin@sig.id',1,'1552',null],
            [1565,'YUDI DARMAWAN','yudi.darmawan','yudi.darmawan@sig.id',1,'1565',null],
            [1591,'MUHAMMAD FAKHRUDIN M., S.Kom.','muhammad.fakhrudin','muhammad.fakhrudin@sig.id',1,'1591',null],
            [1661,'AFIFUDIN ZUHRI, ST.','afifudin.zuhri','afifudin.zuhri@sig.id',1,'1661',null],
            [1662,'RAHMAD ADIE PERDANA, ST.','rahmad.perdana','rahmad.perdana@sig.id',1,'1662',null],
            [1669,'INDRA YUDHI KURNIAWAN','indra.yudhi','indra.yudhi@sig.id',1,'1669',null],
            [1682,'INDAH LUKMAWATI','indah.lukmawati','indah.lukmawati@sig.id',1,'1682',null],
            [1683,'ROGANDA HARIZONA SARAGIH, ST., MM.','roganda.saragih','roganda.saragih@sig.id',1,'1683',null],
            [1707,'OKKY RUBYANZA','okky.rubyanza','okky.rubyanza@sig.id',1,'1707',null],
            [1710,'PUJI DIAH PITALOKA','diah.pitaloka','diah.pitaloka@sig.id',1,'1710',null],
            [1714,'BAYU PRATAMA PUTRA, S.Kom., M.T.','bayu.putra','bayu.putra@sig.id',1,'1714',null],
            [1723,'DEWI FITRI ANGGRAENI','dewi.fitri','dewi.fitri@sig.id',1,'1723',null],
            [1730,'HENDRAWAN TEGUH A., ST., MM.','hendrawan.teguh','hendrawan.teguh@sig.id',1,'1730',null],
            [1741,'RAMA WIJAYA, ST.','rama.wijaya','rama.wijaya@sig.id',1,'1741',null],
            [1745,'ARIEF SETIAWAN, S.Kom., MM., MSc.','arief.setiawan','arief.setiawan@sig.id',1,'1745',null],
            [1750,'CHATUR FEBRIANTO, ST.','chatur.febrianto','chatur.febrianto@sig.id',1,'1750',null],
            [1759,'ZAINAL ARIFIN','zainal.arifin1759','zainal.arifin1759@sig.id',1,'1759',null],
            [1762,'ARIS SODIK, ST.','aris.sodik','aris.sodik@sig.id',1,'1762',null],
            [1772,'EKO SETIAWAN','eko.setiawan','eko.setiawan@sig.id',1,'1772',null],
            [1791,'NURINA PRATIWI SASMITA','nurina.pratiwi','nurina.pratiwi@sig.id',1,'1791',null],
            [1792,'DYAH ISNAINI PUSPITASARI, SE., MM.','dyah.isnaini','dyah.isnaini@sig.id',1,'1792',null],
            [1807,'RISQUL HIDAYAT, ST.','risqul.hidayat','risqul.hidayat@sig.id',1,'1807',null],
            [3064,'GINARYO, ST.','ginaryo','ginaryo@sig.id',1,'3064',null],
            [3195,'AWALUDDIN','awaluddin.3195','awaluddin.3195@sig.id',1,'3195',null],
            [3295,'ISKANDAR SAMUDRA TAQWA, ST.','iskandar.taqwa','iskandar.taqwa@sig.id',1,'3295',null],
            [3513,'RADITYA ALGADRI, ST.','raditya.algadri','raditya.algadri@sig.id',1,'3513',null],
            [3586,'SUPRIYANINGSIH','supriyaningsih','supriyaningsih@sig.id',1,'3586',null],
            [3649,'ARIS SUPRIYATNA, ST., MM.','aris.supriyatna','aris.supriyatna@sig.id',1,'3649',null],
            [3651,'MUHAMMAD SHIDDIQ, ST., MSc.','muhammad.shiddiq','muhammad.shiddiq@sig.id',1,'3651',null],
            [3685,'SARI RAMADHANI, SE.','sari.ramadhani','sari.ramadhani@sig.id',1,'3685',null],
            [3690,'EKO BAGUS PRIYUANTORO, ST.','eko.priyuantoro','eko.priyuantoro@sig.id',1,'3690',null],
            [5100,'M.ASDAR BASIR, SE.','asdar.basir','asdar.basir@sig.id',1,'5100',null],
            [5150,'ARHAM ARAS','arham.aras','arham.aras@sig.id',1,'5150',null],
            [5246,'RENDRA JAKADILAGA, ST.','rendra.jakadilaga','rendra.jakadilaga@sig.id',1,'5246',null],
            [5257,'RENDI TRIHARYO LAKSONO, SE.','rendi.laksono','rendi.laksono@sig.id',1,'5257',null],
            [5290,'ANANTYO PRADHANA PUTRO, ST., MM.','anantyo.pradhana','anantyo.pradhana@sig.id',1,'5290',null],
            [5316,'FANDI HAKIM, ST.','fandi.hakim','fandi.hakim@sig.id',1,'5316',null],
            [5327,'REDI GARJITO, S.Kom., MM.','redi.garjito','redi.garjito@sig.id',1,'5327',null],
            [5403,'ADI AKMAL, SE.','adi.akmal','adi.akmal@sig.id',1,'5403',null],
            [5748,'TIFFANIE TELAMBANUA','tiffanie.telambanua','tiffanie.telambanua@sig.id',1,'5748',null],
            [5901,'MUHLIS SUNUSI','muhlis.sunusi','muhlis.sunusi@sig.id',1,'5901',null],
            [6099,'MUHAMMAD YUHLIS','muhammad.yuhlis','muhammad.yuhlis@sig.id',1,'6099',null],
            [6110,'FAISAL MUNARFAH, S.ST','faisal.munarfah','faisal.munarfah@sig.id',1,'6110',null],
            [6288,'HILMI TAHTA AMRILLAH, ST., MT.','hilmi.tahta','hilmi.tahta@sig.id',1,'6288',null],
            [6291,'RADITYO WIDINUGROHO, SE.','radityo.widinugroho','radityo.widinugroho@sig.id',1,'6291',null],
            [6323,'CHANDRA GUSTAMA YUDHA','chandra.gustama','chandra.gustama@sig.id',1,'6323',null],
            [6336,'JEREMIA MANGAPUL SILITONGA, ST.','jeremia.mangapul','jeremia.mangapul@sig.id',1,'6336',null],
            [6348,'TEGAR GALANG ADILAGA','tegar.galang','tegar.galang@sig.id',1,'6348',null],
            // [6350,'CHANDRA PRADIPTA, SH.','chandra.pradipta','chandra.pradipta@sig.id',1,'6350',null],
            [6353,'ACHMAD AL MAHMUDI, ST.','achmad.mahmudi','achmad.mahmudi@sig.id',1,'6353',null],
            [6355,'ARFEN TRIYUNARTO, ST.','arfen.triyunarto','arfen.triyunarto@sig.id',1,'6355',null],
            [6372,'ACHMAD ALI ASH SHIDIQI','achmad.ali','achmad.ali@sig.id',1,'6372',null],
            [6601,'ELISA MARIS HERU, SE., MM.','elisa.maris','elisa.maris@sig.id',1,'6601',null],
            [6950,'SOLEHUDIN MURPI, ST.','solehudin.murpi','solehudin.murpi@sig.id',1,'6950',null],
            [6964,'DEWI MELANI, M.Psi.','dewi.melani','dewi.melani@sig.id',1,'6964',null],
            [6965,'RIAN DWI CAHYO, ST.','rian.cahyo','rian.cahyo@sig.id',1,'6965',null],
            [6978,'IMRON GOZALI, ST., MT.','imron.gozali','imron.gozali@sig.id',1,'6978',null],
            [6979,'ARY TRI WIBOWO, M.T., CSCM.','ary.wibowo','ary.wibowo@sig.id',1,'6979',null],
            [6980,'SUNGGING HARYO WICAKSONO, Ir., M.T., MSc','sungging.wicaksono','sungging.wicaksono@sig.id',1,'6980',null],
            [6981,'SATRIA NUGRAHA, ST.','satria.nugraha','satria.nugraha@sig.id',1,'6981',null],
            [6987,'MUHAMMAD FADHIL IMANSYAH, ST., MT.','mf.imansyah','mf.imansyah@sig.id',1,'6987',null],
            [6995,'ARIFIANTO, ST.','arifianto','arifianto@sig.id',1,'6995',null],
            // [7001,'MOHAMMAD FIKAR, SE.','mohammad.fikar','mohammad.fikar@sig.id',1,'7001',null],
            [7004,'MUHAMMAD SALIM MARTAK, ST., MM.','muhammad.martak','muhammad.martak@sig.id',1,'7004',null],
            [7006,'ARIFUDDIN WAHYUDI, ST., MT.','arifuddin.wahyudi','arifuddin.wahyudi@sig.id',1,'7006',null],
            [7007,'AWAN YOGATAMA, SE.','awan.yogatama','awan.yogatama@sig.id',1,'7007',null],
            [7009,'AMELIA DJAFAAR, ST.','amelia.djafaar','amelia.djafaar@sig.id',1,'7009',null],
            [7014,'CHUSNUL ASFADILLAH, SE.','chusnul.asfadillah','chusnul.asfadillah@sig.id',1,'7014',null],
            [7051,'ZARAH RAMADANI HARAHAP','zarah.harahap','zarah.harahap@sig.id',1,'7051',null],
            [7064,'MUHAMMAD SHIDQI, S.Ak.','muhammad.shidqi','muhammad.shidqi@sig.id',1,'7064',null],
            [7083,'ILHAM NURDIN, SE.','ilham.nurdin','ilham.nurdin@sig.id',1,'7083',null],
            [7085,'GANTAR WIRAPAKSI, S.A.','gantar.wirapaksi','gantar.wirapaksi@sig.id',1,'7085',null],
            [7141,'FAARISAL HAQ','faarisal.haq','faarisal.haq@sig.id',1,'7141',null],
            [7328,'RAHMAD ARHANU PRIMADI, ST.','rahmad.primadi','rahmad.primadi@sig.id',1,'7328',null],
            [7618,'FELLY TIFANI, SE., MM.','felly.tifani','felly.tifani@sig.id',1,'7618',null],
            [7713,'LINDA MUFARIDA','linda.mufarida','linda.mufarida@sig.id',1,'7713',null],
            [7724,'TRISNA RHAMDHANIYATI','trisna.rhamdhaniyati','trisna.rhamdhaniyati@sig.id',1,'7724',null],
            [7726,'YOPI KISWANTO','yopi.kiswanto','yopi.kiswanto@sig.id',1,'7726',null],
            [7735,'APEL RAMADHAN','apel.ranthy','apel.ranthy@sig.id',1,'7735',null],
            [7740,'NIRMALA ASSIYAH PERBAWATY','nirmala.perbawaty','nirmala.perbawaty@sig.id',1,'7740',null],
            [7744,'ADE HENDRA WIGUNA','ade.wiguna','ade.wiguna@sig.id',1,'7744',null],
            [7754,'ARIS WIRATAMA ISKANDAR','aris.wiratama','aris.wiratama@sig.id',1,'7754',null],
            [7761,'MARIO NORMAN KAURRANY','mario.kaurrany','mario.kaurrany@sig.id',1,'7761',null],
            [7768,'OKTI HARIANINGSIH','okti.harianingsih','okti.harianingsih@sig.id',1,'7768',null],
            [7792,'ERFIN','erfin.sbi','erfin.sbi@sig.id',1,'7792',null],
            [7836,'BUDI PURNAMA','budi.purnama','budi.purnama@sig.id',1,'7836',null],
            [7974,'TUTUK LAKSANA WATI','tutuk.wati','tutuk.wati@sig.id',1,'7974',null],
            [7983,'FREDDY SEVEN PUTRA','freddy.sevenputra','freddy.sevenputra@sig.id',1,'7983',null],
            [7986,'MOHAMMAD KHOIRUL HUDA','m.huda','m.huda@sig.id',1,'7986',null],
            [8018,'AGUS BUDI SANJAYA','agus.sanjaya','agus.sanjaya@sig.id',1,'8018',null],
            [8040,'KHADIJAH UTAMI SABRAN','tami.sabran','tami.sabran@sig.id',1,'8040',null],

        ];
        
    }
    public function userAtasan1()
    {
        return [
            [662,'ERFANTI QODARSIH, SE., Akt., QIA.','erfanti.qodarsih','erfanti.qodarsih@sig.id',1,'662',null],
            [696,'WAHYU DARMAWAN, ST.','wahyu.darmawan','wahyu.darmawan@sig.id',1,'696',null],
            [701,'ACHMAD NURIL, S.Pd.','achmad.nuril','achmad.nuril@sig.id',1,'701',null],
            [737,'EDY SARAYA, ST., MBA., MM.','edy.saraya','edy.saraya@sig.id',1,'737',null],
            [807,'IRMAWATI SRI A., Ir., MT.','irmawati.sri','irmawati.sri@sig.id',1,'807',null],
            [817,'FAJAR SOLEH FAGI EFFENDI, ST.','fajar.soleh','fajar.soleh@sig.id',1,'817',null],
            [996,'EVIE AMALIANA, SE.','evie.amaliana','evie.amaliana@sig.id',1,'996',null],
            [1049,'M. LUDFI SETYADI, Ir.','ludfi.setyadi','ludfi.setyadi@sig.id',1,'1049',null],
            [1066,'GUNTORO, SE.','guntoro.1066','guntoro.1066@sig.id',1,'1066',null],
            [1093,'AGUSWINARTO PUTRADJAKA, PIA.','aguswinarto.p','aguswinarto.p@sig.id',1,'1093',null],
            [1098,'HASAN ARIFIN, SE.','hasan.arifin','hasan.arifin@sig.id',1,'1098',null],
            [1119,'OTTO ANDRI PRIYONO, ST., MM.','otto.priyono','otto.priyono@sig.id',1,'1119',null],
            [1235,'AKHMAD SUJONO, ST.','akhmad.sujono','akhmad.sujono@sig.id',1,'1235',null],
            [1270,'JONI GUNAWAN, S.Sos., MM.','joni.gunawan','joni.gunawan@sig.id',1,'1270',null],
            [1452,'SUHANDIK, ST., M.MT.','suhandik','suhandik@sig.id',1,'1452',null],
            [1457,'MOHAMMAD FAISAL STANY, SH., MM.','mohammad.stany','mohammad.stany@sig.id',1,'1457',null],
            [1548,'AKHMAD YANI YULIANTO, SE., MT., CSCM','akhmad.yulianto','akhmad.yulianto@sig.id',1,'1548',null],
            [1637,'GATHUT WICAKSONO, ST., M.Med.Kom.','gathut.wicaksono','gathut.wicaksono@sig.id',1,'1637',null],
            [1646,'HERU INDRAWIDJAJANTO, Ir., M.MT.','heru.indrawidjajanto','heru.indrawidjajanto@sig.id',1,'1646',null],
            [1689,'YUANITA PURNAMADEWI, SE., MBA.','yuanita.purnamadewi','yuanita.purnamadewi@sig.id',1,'1689',null],
            [1697,'MUHAMMAD TAQIYUDDIN F., S.Kom.','m.taqiyuddin','m.taqiyuddin@sig.id',1,'1697',null],
            [1703,'SIGIT WAHONO, ST., MM.','sigit.wahono','sigit.wahono@sig.id',1,'1703',null],
            [1705,'VITRIA DANIATI, ST.','vitria.daniati','vitria.daniati@sig.id',1,'1705',null],
            [1763,'HALIM ALFATAH, S.Psi., MM.','halim.alfatah','halim.alfatah@sig.id',1,'1763',null],
            [1778,'OKTORIA MASNIARI M.','oktoria.masniari','oktoria.masniari@sig.id',1,'1778',null],
            [1786,'FEBRIANDITA KUSUMA, SE., MFin.','febriandita.kusuma','febriandita.kusuma@sig.id',1,'1786',null],
            [1855,'DWI ANDI MAHENDRA, ST.','dwi.mahendra','dwi.mahendra@sig.id',1,'1855',null],
            [2978,'BENNY ISMANTO, Ir.','benny.ismanto','benny.ismanto@sig.id',1,'2978',null],
            [2990,'AMRAL AHMAD, ST.','amral.ahmad','amral.ahmad@sig.id',1,'2990',null],
            [3576,'MUHAMAD IKRAR, ST., MM.','muhamad.ikrar','muhamad.ikrar@sig.id',1,'3576',null],
            [3642,'FAJAR ARISTYANTO, ST., MT.','fajar.aristyanto','fajar.aristyanto@sig.id',1,'3642',null],
            [3691,'HANNY K. LUKITO, ST.','hanny.lukito','hanny.lukito@sig.id',1,'3691',null],
            [3701,'ROMI ABDILAH, Ir.','romi.abdilah','romi.abdilah@sig.id',1,'3701',null],
            [4530,'SARIATUN, Ir., MM.','sariatun','sariatun@sig.id',1,'4530',null],
            [4536,'AGUS SUBROTO, Ir.','agus.subroto','agus.subroto@sig.id',1,'4536',null],
            [4567,'AHMAD ZULKARNAIN, Ir.','ahmad.zulkarnain','ahmad.zulkarnain@sig.id',1,'4567',null],
            [4901,'SULAIHA MUHYIDDIN, SE, M.BUS, Ak','sulaiha.muhyiddin','sulaiha.muhyiddin@sig.id',1,'4901',null],
            [5237,'MAPPINCARA, S.Kom.','mappincara','mappincara@sig.id',1,'5237',null],
            [5259,'ADE MEIRIYADI, ST.','ade.meiriyadi','ade.meiriyadi@sig.id',1,'5259',null],
            [5305,'MUH.AFANDY MARASABESSY, SE.','muh.marasabessy','muh.marasabessy@sig.id',1,'5305',null],
            [5319,'FARID NUGRAHA, SE.','farid.nugraha','farid.nugraha@sig.id',1,'5319',null],
            [5350,'ENDAH SULISTYO HARYANI, SE., Akt., MSi.','endah.haryani','endah.haryani@sig.id',1,'5350',null],
            [6290,'DEDDY IRFANDY, S.Kom., SE., MBA., Ak.','deddy.irfandy','deddy.irfandy@sig.id',1,'6290',null],
            [6297,'YOSEPH BUDI WICAKSONO, ST., MSc.','yoseph.budi','yoseph.budi@sig.id',1,'6297',null],
            [6332,'NOVA KURNIAWAN, SE.','nova.kurniawan','nova.kurniawan@sig.id',1,'6332',null],
            [6350,'CHANDRA PRADIPTA, SH.','chandra.pradipta','chandra.pradipta@sig.id',1,'6350',null],
            [6429,'ADEL RAHADI RASYID, ST., MCom.','adel.rahadi','adel.rahadi@sig.id',1,'6429',null],
            [7001,'MOHAMMAD FIKAR, SE.','mohammad.fikar','mohammad.fikar@sig.id',1,'7001',null],
            [7287,'RAHMAN KURNIAWAN','rahman.kurniawan','rahman.kurniawan@sig.id',1,'7287',null],
            [7445,'HADI SETIADI','hadi.setiadi','hadi.setiadi@sig.id',1,'7445',null],
            [7469,'PROID KONTURA MARTA','proid.kontura','proid.kontura@sig.id',1,'7469',null],
            [7475,'VITA MAHREYNI','vita.mahreyni','vita.mahreyni@sig.id',1,'7475',null],
            [7651,'JOHANNA DAUNAN','johanna.daunan','johanna.daunan@sig.id',1,'7651',null],
            [7652,'PEGGY FATHIYA','peggy.fathiya','peggy.fathiya@sig.id',1,'7652',null],
            [7663,'ANTONIUS ARDIAN BERMANA','antonius.bermana','antonius.bermana@sig.id',1,'7663',null],
            [7717,'IRMA OKTARINI','irma.oktarini','irma.oktarini@sig.id',1,'7717',null],
            [7741,'DEDE KUSNAWAN','dede.kusnawan','dede.kusnawan@sig.id',1,'7741',null],
            [7752,'SASHA MEDIA','sasha.media','sasha.media@sig.id',1,'7752',null],
            [7780,'RIZKIE AGUSTIANSYAH','rizkie.agustiansyah','rizkie.agustiansyah@sig.id',1,'7780',null],
            [7838,'IKA AGUSTINI','ika.agustini','ika.agustini@sig.id',1,'7838',null],
            [7857,'EKI KARMANTO','eki.karmanto','eki.karmanto@sig.id',1,'7857',null],
            [7873,'IRFAN ARIEF','irfan.arief','irfan.arief@sig.id',1,'7873',null],
            [7877,'HERMAN RINALDI','herman.rinaldi','herman.rinaldi@sig.id',1,'7877',null],
            [7942,'ANDI KRISHNA ARINALDI','andi.krishna','andi.krishna@sig.id',1,'7942',null],
            [7952,'FAJAR ANDHIKA','fajar.andhika','fajar.andhika@sig.id',1,'7952',null],
            [8006,'IRNA RAHMALIA','irna.rahmalia','irna.rahmalia@sig.id',1,'8006',null],
            [8065,'ANINDIO DANESWARA','anindio.daneswara','anindio.daneswara@sig.id',1,'8065',null],
            [8098,'JUHANS SURYANTAN','juhans.suryantan','juhans.suryantan@sig.id',1,'8098',null],
            [8128,'MARALDA HERNANDA KAIRUPAN','maralda.kairupan','maralda.kairupan@sig.id',1,'8128',null],
            [8130,'SUMARLAN WIBAWA','sumarlan.wibawa','sumarlan.wibawa@sig.id',1,'8130',null],
            [8151,'ARIF GUNAWAN SULISTIYONO','arif.gunawan','arif.gunawan@sig.id',1,'8151',null],

        ];
    }
    public function userAtasan2()
    {
        return [
            [7943,'PRAMONO HARJANTO','pramono.harjanto','pramono.harjanto@sig.id',1,'7943',null],


        ];

    }
    public function dirBidang()
    {
        return [
            [8080,'DONNY ARSAL','donny.arsal','donny.arsal@sig.id',1,'8080',null],
            [8095,'AGUNG WIHARTO, S.IP.','agung.wiharto','agung.wiharto@sig.id',1,'8095',null],
            [8192,'YOSVIANDRI, Ir., MM.','yosviandri','yosviandri@sig.id',1,'8192',null],
            [8193,'RENI WULANDARI','reni.wulandari','reni.wulandari@sig.id',1,'8193',null],
            [8083,'SUBHAN, SE., AK., MM., CA','subhan','subhan@sig.id',1,'8083',null],

        ];
    }
    public function dirKeu()
    {
        //id,name,username,email,company_id,no_badge
        return [
            [8093,'ANDRIANO HOSNY PANANGIAN','hosny.panangian','hosny.panangian@sig.id',1,'8093',null],
        ];
    }
}
