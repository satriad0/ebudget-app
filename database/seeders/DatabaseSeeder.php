<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $this->call([
            CurrencySeeder::class,
            CompanySeeder::class,
            // DireksiSeeder::class,
            // CostCenterSeeder::class,
            CostCenterSeeder::class,

            //AdminCoreSeeder::class,
            AdminUserRoleCaseSIG::class,
            //BasicAdminPermissionSeeder::class,
            ClusterSeeder::class,
            GlAccountSeeder::class,
            StatusSeeder::class,
            // UnitKerjaSeeder::class,
            MappingApprovalSIG::class,
            MenuSeeder::class,
        ]);

    }
}
