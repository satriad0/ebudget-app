<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class BasicAdminPermissionSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        $permissions = [
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'writer']);
        $role1->givePermissionTo('permission-list');
        $role1->givePermissionTo('role-list');
        $role1->givePermissionTo('user-list');

        $role2 = Role::create(['name' => 'admin']);
        foreach ($permissions as $permission) {
            $role2->givePermissionTo($permission);
        }

        $role3 = Role::create(['name' => 'super-admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'Super Admin',
            'email' => 'superadmin@example.com',
        ]);
        $user->assignRole($role3);

        $user = \App\Models\User::factory()->create([
            'name' => 'Yopi Satria SuperAdmin',
            'email' => 'yopi.satria@gmail.com',
        ]);
        $user->assignRole($role3);

        $user = \App\Models\User::factory()->create([
            'name' => 'Joko Susilo Admin',
            'email' => 'joko.susilo@gmail.com',
        ]);
        $user->assignRole($role2);

        $user = \App\Models\User::factory()->create([
            'name' => 'Sanjay User',
            'email' => 'sanjay@gmail.com',
        ]);
        $user->assignRole($role1);

        // PAK
        $rolePak = Role::create(['name' => 'pak']);
        $user = \App\Models\User::factory()->create([
            'name' => 'Admin PAK',
            'email' => 'pak@gmail.com',
        ]);
        $user->assignRole($rolePak);
        $roleVpPak = Role::create(['name' => 'vp_pak']);
        $user = \App\Models\User::factory()->create([
            'name' => 'VP PAK',
            'email' => 'vp_pak@gmail.com',
        ]);
        $user->assignRole($roleVpPak);

        // user-user atasan
        $user = \App\Models\User::factory()->create([
            'name' => 'DIRKEU',
            'email' => 'dirkeu@gmail.com',
            'costcenter_id' => 55,
        ]);
        
        $user = \App\Models\User::factory()->create([
            'name' => 'DIREKTORAT OPERASI',
            'email' => 'dir_op@gmail.com',
            'costcenter_id' => 176,
        ]);
        $user = \App\Models\User::factory()->create([
            'name' => 'DEP PROD EXCELLENCE',
            'email' => 'dep_prod@gmail.com',
            'costcenter_id' => 178,
        ]);
        $user = \App\Models\User::factory()->create([
            'name' => 'UNIT PROD PLANNING',
            'email' => 'unit_planing@gmail.com',
            'costcenter_id' => 179,
        ]);
    }
}
