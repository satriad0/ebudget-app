<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\UnitKerja;

class UnitKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        UnitKerja::create([
            'name' => 'Unit Pengawas',
            'company_id' => 1,
            'is_active' => true,
            'created_by' => 1,
            'updated_by' => 0,
            'created_at' => '2020-10-10',
            'updated_at' => '2020-10-10',
        ]);
    }
}
