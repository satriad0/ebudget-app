<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Cluster;

class ClusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $clusters = [
            [
                'id' => 100,
                'cluster_name' => 'Merah',
                'is_active' => true,
                'created_by' => 1,
                'updated_by' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => 200,
                'cluster_name' => 'Hijau',
                'is_active' => true,
                'created_by' => 1,
                'updated_by' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        foreach ($clusters as $cluster) {
            Cluster::create($cluster);
        }
    }
}
