<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $shorts = [
            0 => 'DRAFT',
            1 => 'Revisi',            
            30 =>  'Pengajuan UK Holding',
            40 =>  'Approve Lead 1 UK Holding',
            50 =>  'Approve Lead 2 UK Holding',
            60 =>  'Approve Dir Bidang',
            61 =>  'Revisi PAK',
            70 =>  'Edit PAK',
            80 =>  'Approve VP PAK',
            90 =>  'Approve Dir Bidang V2',
            99 =>  'Approve Dir KEU dg Catatan',
            100 =>  'Approve Dir KEU',
            110 =>  'Reject',
        ];

        foreach ($shorts as $id => $name) {
            Status::create([
                'status_code' => $id,
                'short' => $name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
