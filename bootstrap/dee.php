<?php
defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
defined('ROOT_APP_DIR') or define('ROOT_APP_DIR', dirname(__DIR__));

use Illuminate\Support\Facades\DB;
use yii\console\Application as ConsoleApplication;
use yii\data\Pagination;
use yii\db\Connection;
use yii\web\Application as WebApplication;

require_once dirname(__DIR__) . '/vendor/yiisoft/yii2/Yii.php';

// Yii::$container->set(Pagination::class, [
//     'pageSizeParam' => 'perpage',
// ]);
// ***********************
$config = [
    'id' => 'app',
    'basePath' => dirname(__DIR__),
    'runtimePath' => dirname(__DIR__) . '/storage/runtime',
    'viewPath' => dirname(__DIR__) . '/resources/views',
    'aliases' => [
        '@root' => dirname(__DIR__),
        '@App' => dirname(__DIR__) . '/app',
        '@app' => dirname(__DIR__) . '/app',
    ],
    'bootstrap' => [],
    'components' => [
        'db' => function () {
            $pdo = DB::connection()->getPdo();
            $driverName = DB::connection()->getDriverName();
            $db = new Connection([
                'pdo' => $pdo,
                'driverName' => $driverName,
            ]);
            return $db;
        },
    ],
];

if (PHP_SAPI === 'cli') {
    new ConsoleApplication($config);
} else {
    $config['components']['request'] = [
        'enableCsrfValidation' => false,
        'enableCookieValidation' => false,
        'enableCsrfCookie' => false,
        'parsers' => [
            'application/json' => 'yii\web\JsonParser',
            'multipart/form-data' => 'yii\web\MultipartFormDataParser'
        ]
    ];

    new class($config) extends WebApplication {

            protected function bootstrap()
            {
                yii\base\Application::bootstrap();
            }
        };
}

/**
 * @return Connection
 */
function mDb()
{
    return Yii::$app->db;
}