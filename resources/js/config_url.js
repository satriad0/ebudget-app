export const ConfigUrl = {
    // COMPANY
    getCompany: "/api/company",
    storeCompany: "/api/company-create",
    deleteCompany: "/api/company-delete",
    searchCompany: "/api/company-search",
    updateCompany: "/api/company-update",

    // COST CENTER
    getCostCenter: "/api/cost-center",
    storeCostCenter: "/api/cost-center-create",
    deleteCostCenter: "/api/cost-center-delete",
    searchCostCenter: "/api/cost-center-search",
    updateCostCenter: "/api/cost-center-update",

    // GL ACCOUNT
    getGL: "/api/gl-account",
    storeGL: "/api/gl-account-create",
    deleteGL: "/api/gl-account-delete",
    searchGL: "/api/gl-account-search",
    updateGL: "/api/gl-account-update",

    // CHECK
    checkSSO: "/api/syncSSO",

    // DATA USER
    createSubmission: "/api/create-storesubmission",
    draftCreateSubmission: "/api/draft-create-storesubmission",
    draftSaveSubmission: "/api/draft-save-submission",
    saveSubmission: "/api/save-submission",
    getDatalogin: "/api/get-login",
    getTransDoc: "/api/trans-doc",
    getMasterStatus: "/api/search/status",
    getMasterUser: "/api/get-user",
    getApprovalUk: "/api/approval-uk",
    getreportsubmission: "/api/report-submission",
    getApprovalAtasan: "/api/approval",
    getreportSubmissionView: "/api/report-submissionView",
    getApprove: "/api/approval",
    getApproveDe: "/api/approval-detail1",
    getsubmissionview: "/api/submission/view",
    getsubmissionALlList: "/api/submission",
    getApprovalHistory: "/api/approval-history",
    getTransDocHistory: "/api/trans-doc-history",
    getConstCenter: "/api/user-costcenter",
    getConstCenterAll: "/api/costcenter",
    UpdateSubmission: "/api/update-submission",
    getUser: "/api/user",
    getListDirbidang: "/api/list-dirbidang",
    // DASHBOARD
    getSummary: "/api/summary",
    getCountUser: "/api/dashboard/count/user",
    getAmountAllDocs: "/api/dashboard/amount/all-docs",
    getAmountApproveDocs: "/api/dashboard/amount/approve-docs",
    getCountApproveDocs: "/api/dashboard/count/approve-docs",
    getCountActiveDocs: "/api/dashboard/count/active-docs",
    getCountrejectDocs: "/api/dashboard/count/reject-docs",
    getCountAllDocs: "/api/dashboard/count/all-docs",

    getCCUsers: "/api/cost-center/users",
    getCCUsersAdd: "/api/cost-center/add-users",
    getCCUsersDel: "/api/cost-center/delete-users",
    getCCUpdate: "/api/cost-center-update",
};
