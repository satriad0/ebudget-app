import axios from "axios";
import { usePage } from '@inertiajs/vue3'

const api = axios.create();
api.interceptors.request.use(request => {
    const accessToken = usePage().props.jwt_token;
    if (accessToken) {
        request.headers.Authorization = `Bearer ${accessToken}`;
    }

    const token = usePage().props.remember_token;
    if (token) {
        request.headers.Token = token;
    }

    return request;
});

export default api;