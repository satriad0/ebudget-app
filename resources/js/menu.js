import {
    mdiAccountCircle,
    mdiMonitor,
    mdiGithub,
    mdiAccountKey,
    mdiAccountEye,
    mdiAccountGroup,
    mdiPalette,
} from "@mdi/js";

export default [
    {
        route: "dashboard",
        icon: mdiMonitor,
        label: "Dashboard",
    },
    {
        route: "submissionall",
        icon: mdiMonitor,
        label: "Submission All",
    },
    {
        route: "unfreeze-budget",
        icon: mdiMonitor,
        label: "Unfreeze Budget",
    },
    {
        route: "unfreeze-budget-ap",
        icon: mdiMonitor,
        label: "Unfreeze Budget AP",
    },
    // {
    //     route: "submission",
    //     icon: mdiMonitor,
    //     label: "Submission",
    // },
    {
        route: "HistoryApprove.index",
        icon: mdiMonitor,
        label: "historyapprove",
    },
    {
        route: "ListAllSubmission.index",
        icon: mdiMonitor,
        label: "submissionall",
    },
    {
        route: "approval",
        icon: mdiMonitor,
        label: "Approval",
    },
    {
        route: "mastercost",
        icon: mdiMonitor,
        label: "MasterCost",
    },
    {
        route: "mastergl",
        icon: mdiMonitor,
        label: "MasterGL",
    },
    {
        route: "mastercomp",
        icon: mdiMonitor,
        label: "MasterComp",
    },
    {
        route: "permission.index",
        icon: mdiAccountKey,
        label: "Permissions",
    },
    {
        route: "role.index",
        icon: mdiAccountEye,
        label: "Roles",
    },
    {
        route: "user.index",
        icon: mdiAccountGroup,
        label: "Users",
    },
    {
        href: "https://github.com/balajidharma/laravel-vue-admin-panel",
        label: "GitHub",
        icon: mdiGithub,
        target: "_blank",
    },
];
