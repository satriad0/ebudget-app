<?php

namespace App\Helpers;

/**
 * Format response.
 */
class ResponseFormatter
{
    /**
     * Give success response.
     */
    public static function success($data = null, $message = null)
    {
        return response()->json([
            'codestatus' => 'S',
            'message' => $message,
            'resultdata' => $data,
        ]);
    }

    /**
     * Give error response.
     */
    public static function error($data = null, $message = null)
    {
        return response()->json([
            'codestatus' => 'E',
            'message' => $message,
            'resultdata' => [],
        ]);
    }
}
