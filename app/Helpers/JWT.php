<?php

namespace App\Helpers;

use Firebase\JWT\JWT as FirebaseJWT;
use Firebase\JWT\Key;

class JWT
{

    protected static function getSecret()
    {
        return env('JWT_SECRET', md5(__FILE__));
    }

    public static function encode($payload = [])
    {
        if (empty($payload['iat'])) {
            $payload['iat'] = time();
        }
        if (empty($payload['exp'])) {
            $payload['exp'] = time() + 30 * 24 * 3600;
        }
        return FirebaseJWT::encode($payload, static::getSecret(), 'HS256');
    }

    public static function decode($token)
    {
        return FirebaseJWT::decode($token, new Key(static::getSecret(), 'HS256'));
    }
    
}
