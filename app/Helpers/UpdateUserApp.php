<?php

namespace App\Helpers;

use App\Models\User;
use App\Models\UserInformation;
use Illuminate\Support\Facades\Hash;

class UpdateUserApp
{
    public function handle($data, User $user,$userInfoData): User
    {
        $user->update([
            'name' => $data->name,
            'email' => $data->email,
            'company_id' => 1,/* data->company_id */
            'costcenter_id' => 1 /* data->costcenter_id */,
            'is_user_sso' => 't'/* data->is_user_sso */,
            'is_user_ldap' => 'f'/* data->is_user_ldap */,
        ]);

        if ($data->password) {
            $user->update([
                'password' => Hash::make($data->password),
            ]);
        }

        
        $roles = $data->roles ?? [];
        $user->syncRoles($roles);
        $UsrInfo = UserInformation::where('user_id', $user->id);
        if ($UsrInfo) {

            // upload Foto to Storage
            // $file = $data->file('image');
            // $nama = $user->id.'_'.time();
            // $extension = $file->getClientOriginalExtension();
            // $newName = $nama .'.'.$extension;

            // $path = Storage::putFileAs('user_photo',$data->file('image'),$newName);


            $UsrInfo->update([
                'first_name' =>'',
                'last_name' => '',
                'place_of_birth' => $data->place_of_birth,//'Gresik',/* data->company_id */
                'date_of_birth' => $data->date_of_birth,//'1995-02-01' /* data->costcenter_id */,
                'gender' => $data->gender,//'L'/* data->is_user_sso */,
                'address' => $data->address,
                'image_url' => null //$user->id.'_foto.png',
            ]);
        }else{
            UserInformation::create([
                'user_id' => $user->id,
                'first_name' => '',
                'last_name' => '',
                'place_of_birth' => $data->place_of_birth,//'Gresik',/* data->company_id */
                'date_of_birth' => $data->date_of_birth,//'1995-02-01' /* data->costcenter_id */,
                'gender' => $data->gender,//'L'/* data->is_user_sso */,
                'image_url' => null,//'tes.png',
                'address' => $data->address,//'Jalan Patimura No 28'/* data->is_user_ldap */,
            ]);
        }

        return $user;
    }
}
