<?php

namespace App\Helpers;

use App\Models\User;
use App\Models\UserInformation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class CreateUserApp
{
    public function handle($data): User
    {
        // 'password' => Hash::make($data->password),
        // get username from email user before @
        
        $username = explode('@',$data->email);
        $user = User::factory()->create([
            'id' => $data->nip,
            'name' => $data->name,
            'email' => $data->email,
            'password' => Hash::make('password'),
            'username' => $username[0],
            'no_badge' => $data->nip,
            'company_id' => 1,/* data->company_id */
            'costcenter_id' => 1 /* data->costcenter_id */,
            'is_user_sso' => 1/* data->is_user_sso */,
            'is_user_ldap' => 1/* data->is_user_ldap */,
        ]);

        if ($user) {

            // upload Foto to Storage
            // $file = $data->file('image');
            // $nama = $user->id.'_'.time();
            // $extension = $file->getClientOriginalExtension();
            // $newName = $nama .'.'.$extension;

            // $path = Storage::putFileAs('user_photo',$data->file('image'),$newName);

            $userInfo = UserInformation::create([
                'user_id' => $data->nip,
                'first_name' => '',
                'last_name' => '',
                'place_of_birth' => $data->place_of_birth,//'Gresik',/* data->company_id */
                'date_of_birth' => $data->date_of_birth,//'1995-02-01' /* data->costcenter_id */,
                'gender' => $data->gender,//'L'/* data->is_user_sso */,
                'image_url' => 'tes.png',
                'address' => $data->address,//'Jalan Patimura No 28'/* data->is_user_ldap */,
            ]);
        }


        $roles = $data->roles ?? [];
        $user->assignRole($roles);

        return $user;
    }
}
