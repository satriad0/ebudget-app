<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApprovalHistory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'trans_doc_id',
        'action',
        'action_at',
        'action_by',
        'doc_status',
        'prev_doc_status',
        'note',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
