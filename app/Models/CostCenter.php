<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostCenter extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'cost_center_code',
        'description',
        'hirarchy',
        'cost_center_category',
        'company_id',
        'currency_id',
        'profit_center',
        'user_input_id',
        'user_atasan_id',
        'user_atasan2_id',
        'user_dir_bidang_id',
        'user_dirkeu_id',
        'cost_center_category2',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
    ];
}
