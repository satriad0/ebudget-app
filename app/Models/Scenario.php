<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scenario extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'scenario_name',
        'tahun',
        'trans_docs_id',
    ];

    public function transDoc()
    {
        return $this->belongsTo(TransDoc::class, 'trans_docs_id');
    }

    public function beban()
    {
        return $this->hasMany(Beban::class, 'scenario_id');
    }
}
