<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'company_code',
        'company_name',
        'address',
        'country',
        'currency_id',
        'hirarchy',
        'FM_Area',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
    ];

    public function company()
    {
       return $this->hasMany(User::class);
    }
}
