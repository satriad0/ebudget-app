<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInformation extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [ 
        'user_id',
        'place_of_birth',
        'first_name',
        'last_name',
        'place_of_birth',
        'date_of_birth',
        'gender',
        'address',
        'image_url'
    ];

    protected $guarded = [];
}
