<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransDoc extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'company_id',
        'costcenter_id',
        'dir_bidang_id',
        'currency_id',
        'date',
        'number',
        'is_holding',
        'tahun',
        'status_id',
        'aktifitas',
        'desc_aktifitas',
        'latar_belakang',
        'ebitda',
        'opex',
        'dampak',
        'resiko',
        'doc_desc',
        'files',
        'is_kaitan_langsung',
        'is_kaitan_tidak_langsung',
        'point_perhatian_pak',
        'dampak_pak',
        'resiko_pak',
        'rekomendasi_pak',
        'dampak_atas',
        'dampak_tidak_sesuai_ajuan',
        'dampak_tidak_sesuai_realisasi',
        'dampak_sesuai_rekomendasi',
        'rekomendasi_tambah',
        'user_atasan',
        'user_atasan2',
        'user_dir_bidang',
        'user_dir_bidang_v2',
        'user_dirkeu',
        'created_by',
        'input_data',
    ];

    public function costCenter()
    {
        return $this->belongsTo(CostCenter::class, 'costcenter_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function scenarios()
    {
        return $this->hasMany(Scenario::class, 'trans_docs_id');
    }
}
