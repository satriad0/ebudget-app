<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostCenterUserInput extends Model
{
    protected $fillable = [
        'costcenter_id',
        'user_id',
    ];
    use HasFactory;
}
