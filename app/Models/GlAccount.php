<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GlAccount extends Model
{
    use HasFactory, SoftDeletes;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'gl_account',
        'description',
        'account_type',
        'cf_acc',
        'cf_tb',
        'contra_acc1',
        'dimlist',
        'fs_structure',
        'group',
        'rate_type',
        'type_elim',
        'parenth1',
        'gl_id',
        'last_update',
        'cash_flow',
        'group_cycle',
        'structure_costing',
        'group_hrgl_',
        'created_by',
        'updated_by',
    ];
}
