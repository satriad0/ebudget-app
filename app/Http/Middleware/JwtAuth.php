<?php

namespace App\Http\Middleware;

use App\Helpers\JWT;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JwtAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!auth()->guest()) {
            return $next($request);
        }
        $token = $request->header('Authorization');
        if ($token && preg_match('/^Bearer\s+(.+)/', $token, $match)) {
            $jwt = $match[1];
            try {
                $payload = JWT::decode($jwt);
                $user = User::where('id', $payload->sub)->first();
                if ($user) {
                    auth()->login($user);
                    return $next($request);
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        if ($request->route()->getName() == 'request-jwt-token') {
            return $next($request);
        }
        if (env('AUTH_TEST') && ($uId = $request->query('_token'))) {
            $user = User::where('id', $uId)->first();
            if ($user) {
                auth()->login($user);
                return $next($request);
            }
        }
        abort(401, 'Unauthorize');
    }
}
