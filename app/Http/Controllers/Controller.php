<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @SWG\Swagger(
 *     basePath="/",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="EBudget API Docs",
 *         description="Dokumentasi API untuk EBudget",
 *     ),
 *     schemes={"https","http"},
 *     @SWG\SecurityScheme(
 *         securityDefinition="token",
 *         type="apiKey",
 *         name="Authorization",
 *         in="header"
 *     ),
 * )
 *
 * @SWG\Definition(
 *   definition="Pagination",
 *   @SWG\Property(property="page", type="integer"),
 *   @SWG\Property(property="perpage", type="integer"),
 *   @SWG\Property(property="total", type="integer"),
 * )
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
