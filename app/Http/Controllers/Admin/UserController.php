<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CreateUserApp;
use App\Helpers\Requests\UpdateUserRequestApp as RequestsUpdateUserRequestApp;
use App\Helpers\StoreUserRequestApp;
use App\Helpers\UpdateUserApp;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Models\UserInformation;
use App\Requests\Helpers\UpdateUserRequest as HelpersUpdateUserRequest;
use App\Requests\Helpers\UpdateUserRequestApp;
use BalajiDharma\LaravelAdminCore\Actions\User\CreateUser;
use BalajiDharma\LaravelAdminCore\Actions\User\UpdateUser;
use BalajiDharma\LaravelAdminCore\Requests\StoreUserRequest;
use BalajiDharma\LaravelAdminCore\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:user-list', ['only' => ['index', 'show']]);
        $this->middleware('can:user-create', ['only' => ['create', 'store']]);
        $this->middleware('can:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('can:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */


    public function index()
    {

        // $users = User::join('companies', 'companies.id', '=', 'users.company_id')
        // ->join('cost_centers', 'cost_centers.id', '=', 'users.costcenter_id')->first();
        
        $users = (new User)->newQuery();

        if (request()->has('search')) {
            $users->where('name', 'ILike', '%'.request()->input('search').'%')
            ->orWhere('no_badge', 'ILike', '%'.request()->input('search').'%' )
            ->orWhere('email', 'ILike', '%'.request()->input('search').'%' );
        }

        // dd($users);
        
        if (request()->query('sort')) {
            $attribute = request()->query('sort');
            $sort_order = 'ASC';
            if (strncmp($attribute, '-', 1) === 0) {
                $sort_order = 'DESC';
                $attribute = substr($attribute, 1);
            }
            $users->orderBy($attribute, $sort_order);
        } 
        else {
            $users->latest();
        }
        
        $users = $users->with('company')->paginate(5)->onEachSide(2)->appends(request()->query());

        // dd($users->toArray());
        return Inertia::render('Admin/User/Index', [
            'users' => $users,
            'filters' => request()->all('search'),
            'nav_menu' => \App\Http\Requests\Auth\LoginRequest::getMenu(),
            'can' => [
                'create' => Auth::user()->can('user-create'),
                'edit' => Auth::user()->can('user-edit'),
                'delete' => Auth::user()->can('user-delete'),
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {

        $roles = Role::all()->pluck("name","id");

        return Inertia::render('Admin/User/Create', [
            'roles' => $roles,
            'nav_menu' => \App\Http\Requests\Auth\LoginRequest::getMenu(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUserRequest  $request
     * @param  CreateUser  $createUser
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CreateUserApp $createUser)
    {

        $createUser->handle((object) $request->all());
        return redirect()->route('user.index')
                        ->with('message', __('User created successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Inertia\Response
     */
    public function show(User $user)
    {
        $roles = Role::all()->pluck("name","id");
        $userHasRoles = array_column(json_decode($user->roles, true), 'id');

        $user  = User::join('companies', 'companies.id', '=', 'users.company_id')
        ->join('cost_centers', 'cost_centers.id', '=', 'users.costcenter_id')
        ->join('user_information', 'users.id', '=', 'user_information.user_id')
        ->where('users.id',$user->id)->first();


        return Inertia::render('Admin/User/Show', [
            'user' => $user,
            'nav_menu' => \App\Http\Requests\Auth\LoginRequest::getMenu(),
            'roles' => $roles,
            'userHasRoles' => $userHasRoles,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Inertia\Response
     */
    public function edit(User $user, UserInformation $userInformation)
    {
        $roles = Role::all()->pluck("name","id");
        $userHasRoles = array_column(json_decode($user->roles, true), 'id');
       
        $UsrInfo = User::select('user_information.*')->leftJoin('user_information', function($join) {
            $join->on('users.id', '=', 'user_information.user_id');
          })
          ->where('users.id',$user->id)->get();

        return Inertia::render('Admin/User/Edit', [
            'user' => $user,
            'nav_menu' => \App\Http\Requests\Auth\LoginRequest::getMenu(),
            'user_info' => $UsrInfo,
            'roles' => $roles,
            'userHasRoles' => $userHasRoles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RequestsUpdateUserRequestApp  $request
     * @param  \App\Models\User  $user
     * @param  UpdateUserApp  $updateUser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestsUpdateUserRequestApp $request, User $user, UpdateUserApp $updateUser)
    {   
        $userInformation = User::join('companies', 'companies.id', '=', 'users.company_id')
        ->join('cost_centers', 'cost_centers.id', '=', 'users.costcenter_id')
        ->join('user_information', 'users.id', '=', 'user_information.user_id')
        ->where('users.id',$user->id)->get();
        
        $updateUser->handle((object) $request->all(), $user,$userInformation);

        return redirect()->route('user.index')
                        ->with('message', __('User updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.index')
                        ->with('message', __('User deleted successfully'));
    }

    /**
     * Show the user a form to change their personal information & password.
     *
     * @return \Inertia\Response
     */
    public function accountInfo()
    {
        $user = \Auth::user();

        return Inertia::render('Admin/User/AccountInfo', [
            'user' => $user,
            'nav_menu' => \App\Http\Requests\Auth\LoginRequest::getMenu(),
        ]);
    }

    /**
     * Save the modified personal information for a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function accountInfoStore(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.\Auth::user()->id],
        ]);

        $user = \Auth::user()->update($request->except(['_token']));

        if ($user) {
            $message = 'Account updated successfully.';
        } else {
            $message = 'Error while saving. Please try again.';
        }

        return redirect()->route('admin.account.info')->with('message', __($message));
    }

    /**
     * Save the new password for a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePasswordStore(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'old_password' => ['required'],
            'new_password' => ['required', Rules\Password::defaults()],
            'confirm_password' => ['required', 'same:new_password', Rules\Password::defaults()],
        ]);

        $validator->after(function ($validator) use ($request) {
            if ($validator->failed()) {
                return;
            }
            if (! Hash::check($request->input('old_password'), \Auth::user()->password)) {
                $validator->errors()->add(
                    'old_password', __('Old password is incorrect.')
                );
            }
        });

        $validator->validate();

        $user = \Auth::user()->update([
            'password' => Hash::make($request->input('new_password')),
        ]);

        if ($user) {
            $message = 'Password updated successfully.';
        } else {
            $message = 'Error while saving. Please try again.';
        }

        return redirect()->route('admin.account.info')->with('message', __($message));
    }
}
