<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TransDoc;
use App\Models\Burden;
use App\Models\User;
use App\Models\Scenario;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use App\Helpers\ResponseFormatter;
use App\Models\ApprovalHistory;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use Illuminate\Support\Facades\Mail;

class ApprovalController extends Controller
{

    /**
     * @SWG\Post(path="/api/approval-atasan",
     *     tags={"approval"},
     *     summary="List dokumen yang akan diapprove.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="query", name="holding", type="string", enum={"uk","pt"}),
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(
     *             @SWG\Property(property="page", type="integer"),
     *             @SWG\Property(property="perpage", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "List dokumen",
     *         @SWG\Schema(type="boolean")
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;
        $isPAK = $user->hasRole('pak');
        $isVP_PAK = $user->hasRole('vp_pak');

        if ($isPAK) {
            $isApprove = ['td.status_id' => [60, 61, 99]];
        } elseif ($isVP_PAK) {
            $isApprove = ['td.status_id' => 70];
        } else {
            $isApprove = [
                'OR',
                ['td.user_atasan' => $userId, 'td.status_id' => 30],
                ['td.user_atasan2' => $userId, 'td.status_id' => 40],
                ['td.user_dir_bidang' => $userId, 'td.status_id' => 50],
                ['td.user_dir_bidang_v2' => $userId, 'td.status_id' => 80],
                ['td.user_dirkeu' => $userId, 'td.status_id' => 90],
            ];
        }

        $query = (new Query)
            ->select([
                'td.*', 'st.short',
                'st.short name_status', 'cc.description cost_center_name',
                'c.company_name', 'dir.description dir_bidang_name'
            ])
            ->from('trans_docs td')
            ->leftJoin('statuses st', 'st.status_code=td.status_id')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->orderBy(['td.number' => SORT_DESC]);
        $query->andWhere($isApprove);
        if (($holding = $request->query('holding'))) {
            $query->andWhere(['td.is_holding' => ($holding == 'uk')]);
        }
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => request('page') - 1,
                'pageSize' => request('perpage'),
            ]
        ]);

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => $provider->totalCount . ' Data Found',
            'resultdata' => $provider->getModels(),
            'pagination' => [
                'perpage' => $provider->pagination->pageSize,
                'page' => $provider->pagination->page + 1,
                'totaldata' => $provider->totalCount,
            ],
        ];

        return response()->json($response);
    }

    /**
     * @SWG\Post(path="/api/approval-detail",
     *     tags={"approval"},
     *     summary="View dokumen yang akan diapprove.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="query", name="id", type="integer"),
     *     @SWG\Response(
     *         response = 200,
     *         description = "View dokumen",
     *         @SWG\Schema(type="boolean")
     *     ),
     *     security={{"token": {}}},
     * )
     *
     * @SWG\Post(path="/submission/view",
     *     tags={"submission"},
     *     summary="View dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="query", name="id", type="integer"),
     *     @SWG\Response(
     *         response = 200,
     *         description = "View dokumen",
     *         @SWG\Schema(type="boolean")
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function view(Request $request, $id = null)
    {
        if ($id == null) {
            $id = $request->id;
        }
        $user = auth()->user();
        $transDoc = (new Query)
            ->select([
                'td.*', 'st.short',
                'st.short name_status', 'cc.description cost_center_name',
                'c.company_name', 'dir.description dir_bidang_name'
            ])
            ->from('trans_docs td')
            ->leftJoin('statuses st', 'st.status_code=td.status_id')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->where(['td.id' => $id])
            ->one();
        // dd($transDoc);
        if (!$transDoc) {
            abort(404, 'Not Found');
            return;
        }

        $status = $transDoc['status_id'];

        $userId = $user->id;
        $isPAK = $user->hasRole('pak');
        $isVP_PAK = $user->hasRole('vp_pak');

        $hasAccess = $this->hasAccessCC($transDoc['costcenter_id'], $userId);
        $isApprove = ($isVP_PAK && $status == 70) ||
            ($transDoc['user_atasan'] == $userId && $status == 30) ||
            ($transDoc['user_atasan2'] == $userId && $status == 40) ||
            ($transDoc['user_dir_bidang'] == $userId && $status == 50) ||
            ($transDoc['user_dir_bidang_v2'] == $userId && $status == 80) ||
            ($transDoc['user_dirkeu'] == $userId && $status == 90);
        $valid = ($userId == $transDoc['created_by']) ||
            ($hasAccess) ||
            ($isPAK && ($status == 60 || $status == 61 || $status == 99)) ||
            $isApprove;

        if (!$valid) {
            abort(403, 'Forbiden');
            return;
        }

        $editable = (($userId == $transDoc['created_by'] || $hasAccess) && ($status == 0 || $status == 1)) ||
            ($isPAK && ($status == 60 || $status == 61 || $status == 99));
        // Mengecek status dokumen dan mengatur nilai tombol
        $statusDoc = [
            'button_approve' => $isApprove,
            'button_reject' => ($isApprove && $status != 70),
            'button_revisi' => $isApprove,
            'button_approvereason' => false, //($isApprove && $status == 90),
            'button_submit_edit' => $editable,
            'button_submit_save' => $editable,
            'button_save_draft' => $editable,
            'editable' => $editable,
            'show_rekomendasi' => $status >= 60,
        ];
        $histories = (new Query)
            ->select([
                'ah.action', 'ah.action_at', 'ah.action_by',
                'ah.doc_status status_id', 'ah.prev_doc_status prev_status_id',
                'ah.note', 'ah.note_dirkeu',
                'u.name as action_by_name', 's1.short as status', 's2.short as prev_status'
            ])
            ->from('approval_histories ah')
            ->leftJoin('statuses s1', 's1.status_code=ah.doc_status')
            ->leftJoin('statuses s2', 's2.status_code=ah.prev_doc_status')
            ->leftJoin('users u', 'u.id=ah.action_by')
            ->where(['ah.trans_doc_id' => $id])
            ->orderBy(['ah.action_at' => SORT_DESC])
            ->all();
        $data_support = [
            'trans_doc_id' => $transDoc['id'],
            'doc_desc' => $transDoc['doc_desc'],
            'files' => json_decode($transDoc['files'], true),
            'status_doc' => $status,
            'name_status' => $transDoc['name_status'],
        ];

        // Membuat array data TransDoc
        $data = array_merge($transDoc, [
            'aktivitas' => $transDoc['aktifitas'],
            'description' => $transDoc['desc_aktifitas'],
            'activity_background' => $transDoc['latar_belakang'],
            'dampak_aktivitas' => $transDoc['dampak'],
            'skenario' => []
        ]);
        $rows = (new Query)
            ->select([
                'sc.id', 'sc.scenario_name', 'b.*',
            ])
            ->from('scenarios sc')
            ->leftJoin('burdens b', 'b.scenario_id=sc.id')
            ->where(['sc.trans_docs_id' => $id])
            ->orderBy(['sc.id' => SORT_ASC, 'b.id' => SORT_ASC])
            ->all();
        $scenarios = [];
        foreach ($rows as $row) {
            $scId = $row['id'];
            if (!isset($scenarios[$scId])) {
                $scenarios[$scId] = [
                    'skenario_name' => $row['scenario_name'],
                    'beban' => [],
                    'total_bulan' => [
                        1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
                        7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0,
                    ]
                ];
            }
            $scenarios[$scId]['total_bulan'][$row['bulan']] += $row['nominal'];
            $scenarios[$scId]['beban'][] = [
                'aktifitas_program' => $row['aktifitas_program'],
                'cost_center' => $row['cost_center'],
                'cost_element' => $row['cost_element'],
                'akun' => $row['akun'],
                'komponen_biaya' => $row['komponen_biaya'],
                'bulan' => $row['bulan'],
                'nominal' => $row['nominal']
            ];
        }
        $data['skenario'] = $scenarios;

        $response = [
            'codestatus' => 'S',
            'message' => 'Success get data',
            'resultdata' => [
                'statusdoc' => $statusDoc,
                'transdoc' => $data,
                'data_support' => $data_support,
                'data' => json_decode($transDoc['input_data']),
                'histories' => $histories,
                'data_cost_center' => $transDoc['cost_center_name']
            ]
        ];

        return response()->json($response);
    }

    /**
     * @param int $cc_id
     * @param int $user_id
     * @return bool
     */
    protected function hasAccessCC($cc_id, $user_id)
    {
        return (new Query)
            ->select(['cc.*'])
            ->from('cost_centers cc')
            ->leftJoin('cost_center_user_inputs cu', 'cu.costcenter_id=cc.id')
            ->where(['OR',
                ['cu.user_id' => $user_id],
                ['cc.user_input_id' => $user_id],
            ])
            ->andWhere(['cc.id' => $cc_id])
            ->exists();
    }



    /**
     * @SWG\Post(path="/approval/{id}/{action}",
     *     tags={"approval"},
     *     summary="Approve dokumen yang akan diapprove.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="path", name="id", type="integer"),
     *     @SWG\Parameter(in="path", name="action", type="string", enum={"approve","approvereason","revisi","reject"}),
     *     @SWG\Response(
     *         response = 200,
     *         description = "View dokumen",
     *         @SWG\Schema(type="boolean")
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function store(Request $request, $id, $action)
    {
        $user = auth()->user();
        $userId = $user->id;

        $transDoc = (new Query)
            ->select([
                'td.*',
                'st.short name_status', 'cc.description data_cost_center',
                'c.company_name', 'dir.description dir_bidang_name',
                'u.email',
            ])
            ->from('trans_docs td')
            ->leftJoin('statuses st', 'st.status_code=td.status_id')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('users u', 'td.created_by=u.id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->where(['td.id' => $id])
            ->one();
        $status = $transDoc['status_id'];
        $approveTo = $revisiTo = $approveReasonTo = null;
        switch ($status) {
            case 30:
                $valid = ($userId == $transDoc['user_atasan']);
                if (!$valid) {
                    abort(403, "Forbiden");
                    return;
                }
                if ($transDoc['user_atasan2']) {
                    $approveTo = 40;
                } elseif ($transDoc['user_dir_bidang'] && $transDoc['user_dir_bidang'] != $transDoc['user_dirkeu']) {
                    $approveTo = 50;
                } else {
                    $approveTo = 60;
                }
                $revisiTo = 1;
                break;
            case 40:
                $valid = ($userId == $transDoc['user_atasan2']);
                if (!$valid) {
                    abort(403, "Forbiden");
                    return;
                }
                if ($transDoc['user_dir_bidang'] && $transDoc['user_dir_bidang'] != $transDoc['user_dirkeu']) {
                    $approveTo = 50;
                } else {
                    $approveTo = 60;
                }
                $revisiTo = 1;
                break;
            case 50:
                $valid = ($userId == $transDoc['user_dir_bidang']);
                if (!$valid) {
                    abort(403, "Forbiden");
                    return;
                }
                $approveTo = 60;
                $revisiTo = 1;
                break;
            case 70:
                if (!$user->hasRole('vp_pak')) {
                    abort(403, "Forbiden");
                    return;
                }
                $approveTo = 90;
                $revisiTo = $transDoc['is_holding'] ? 61 : 1;
                break;
            case 90:
                if ($user->id != $transDoc['user_dirkeu']) {
                    abort(403, "Forbiden");
                    return;
                }
                $approveTo = 100;
                $revisiTo = $transDoc['is_holding'] ? 61 : 1;
                $approveReasonTo = 99;
                break;
        }

        switch ($action) {
            case 'approve':
                $newStatus = $approveTo;
                break;
            case 'approvereason':
                $newStatus = $approveReasonTo;
                break;
            case 'revisi':
                $newStatus = $revisiTo;
                break;
            case 'reject':
                $newStatus = 110;
                break;
        }

        DB::transaction(function () use ($id, $newStatus, $action, $user, $request, $status) {
            $command = mDb()->createCommand();
            $res = $command->update('trans_docs', [
                'status_id' => $newStatus,
            ], ['id' => $id])->execute();
            if ($res) {
                $command->insert('approval_histories', [
                    'trans_doc_id' => $id,
                    'action' => $action,
                    'action_at' => Carbon::now(),
                    'action_by' => $user->id,
                    'doc_status' => $newStatus,
                    'prev_doc_status' => $status,
                    'note' => $request->post('reason'),
                    'note_dirkeu' => $request->post('reasondirkeu'),
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ])->execute();
            }
        });

        if(env('SEND_EMAIL')){
            Mail::send('notifikasi', ['action' => $action, 'status' => $status, 'aktivitas' => $transDoc['aktifitas'], 'number' => $transDoc['number']], function ($message) use ($transDoc) {
                $message->from(env('MAIL_FROM_ADDRESS'));
                $message->to($transDoc['email']);
                $message->subject('Approval OPEX');
            });
        }

        $response = [
            'codestatus' => 'S',
            'message' => 'Success approve data',
        ];

        return response()->json($response);
    }

    public function history(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;

        $query = (new Query)
            ->select([
                'ah.*', 'st1.short name_status', 'st2.short name_prev_status',
                'td.company_id',
                'td.costcenter_id',
                'td.dir_bidang_id',
                'td.currency_id',
                'td.date',
                'td.number',
                'td.is_holding',
                'td.tahun',
                'td.aktifitas',
                'td.desc_aktifitas',
                'td.latar_belakang',
                'td.ebitda',
                'td.opex',
                'td.dampak',
                'td.resiko',
                'td.files',
                'td.is_kaitan_langsung',
                'td.is_kaitan_tidak_langsung',
                'td.point_perhatian_pak',
                'td.dampak_pak',
                'td.resiko_pak',
                'td.rekomendasi_pak',
                'td.dampak_atas',
                'td.dampak_tidak_sesuai_ajuan',
                'td.dampak_tidak_sesuai_realisasi',
                'td.dampak_sesuai_rekomendasi',
                'td.rekomendasi_tambah',
                'td.created_by owner_id',
                'cc.description cost_center_name',
                'c.company_name', 'dir.description dir_bidang_name'
            ])
            ->from('approval_histories ah')
            ->innerJoin('trans_docs td', 'ah.trans_doc_id=td.id')
            ->leftJoin('statuses st1', 'st1.status_code=ah.doc_status')
            ->leftJoin('statuses st2', 'st2.status_code=ah.prev_doc_status')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->andWhere(['ah.action_by' => $userId])
            ->orderBy(['ah.action_at' => SORT_DESC]);

        $query->andFilterWhere([
            'td.created_by' => $request->query('created_by'),
            'td.costcenter_id' => $request->query('costcenter_id'),
            'td.status_id' => $request->query('status'),
            'td.company_id' => $request->query('company_id'),
        ]);

        $query->andFilterWhere(['ilike', 'td.number', $request->query('number')]);
        if ($q = $request->query('q')) {
            $query->andWhere([
                'OR',
                ['ilike', 'td.aktifitas', $q],
                ['ilike', 'td.desc_aktifitas', $q],
                ['ilike', 'td.latar_belakang', $q],
                ['ilike', 'td.dampak', $q],
                ['ilike', 'td.resiko', $q],
                ['ilike', 'td.resiko_pak', $q],
                ['ilike', 'td.rekomendasi_pak', $q],
            ]);
        }

        if (($holding = $request->query('holding'))) {
            $query->andWhere(['td.is_holding' => ($holding == 'uk')]);
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => request('page') - 1,
                'pageSize' => request('perpage'),
            ]
        ]);

        $rows = array_map(function ($v) {
            if ($files = json_decode($v['files'], true)) {
                $v['files'] = $files;
            }
            return $v;
        }, $provider->getModels());
        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => $provider->totalCount . ' Data Found',
            'resultdata' => $rows,
            'pagination' => [
                'perpage' => $provider->pagination->pageSize,
                'page' => $provider->pagination->page + 1,
                'totaldata' => $provider->totalCount,
            ],
        ];

        return response()->json($response);
    }
}
