<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TransDoc;
use App\Models\Burden;
use App\Models\User;
use App\Models\Scenario;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use App\Helpers\ResponseFormatter;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class SubmissionController extends Controller
{

    /**
     * @SWG\Post(path="/api/submission",
     *     tags={"submission"},
     *     summary="List dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="query", name="q", type="string"),
     *     @SWG\Parameter(in="query", name="number", type="string"),
     *     @SWG\Parameter(in="query", name="holding", type="string", enum={"uk","pt"}),
     *     @SWG\Parameter(in="query", name="date", type="string"),
     *     @SWG\Parameter(in="query", name="tahun", type="integer"),
     *     @SWG\Parameter(in="query", name="costcenter_id", type="integer"),
     *     @SWG\Parameter(in="query", name="status", type="integer"),
     *     @SWG\Parameter(in="query", name="company_id", type="integer"),
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(
     *             @SWG\Property(property="page", type="integer"),
     *             @SWG\Property(property="perpage", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "List dokumen",
     *         @SWG\Schema(type="boolean")
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;

        $qUserInput = (new Query)
            ->select(['costcenter_id'])
            ->from('cost_center_user_inputs')
            ->where(['user_id' => $userId]);
        $query = (new Query)
            ->select([
                'td.*', 'st.short',
                'st.short name_status', 'cc.description cost_center_name',
                'c.company_name', 'dir.description dir_bidang_name'
            ])
            ->from('trans_docs td')
            ->leftJoin('statuses st', 'st.status_code=td.status_id')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->orderBy(['td.number' => SORT_DESC]);

        $query->andWhere([
            'OR',
            ['td.created_by' => $userId],
            ['IN', 'td.costcenter_id', $qUserInput]
        ]);

        $query->andFilterWhere([
            'td.date' => $request->query('date'),
            'td.tahun' => $request->query('tahun'),
            'td.costcenter_id' => $request->query('costcenter_id'),
            'td.status_id' => $request->query('status'),
            'td.company_id' => $request->query('company_id'),
        ]);

        $query->andFilterWhere(['ilike', 'td.number', $request->query('number')]);
        if ($q = $request->query('q')) {
            $query->andWhere([
                'OR',
                ['ilike', 'td.aktifitas', $q],
                ['ilike', 'td.desc_aktifitas', $q],
                ['ilike', 'td.latar_belakang', $q],
                ['ilike', 'td.dampak', $q],
                ['ilike', 'td.resiko', $q],
                ['ilike', 'td.resiko_pak', $q],
                ['ilike', 'td.rekomendasi_pak', $q],
            ]);
        }
        if (($holding = $request->query('holding'))) {
            $query->andWhere(['td.is_holding' => ($holding == 'uk')]);
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => intval(request('page')) - 1,
                'pageSize' => request('perpage'),
            ]
        ]);

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => $provider->totalCount . ' Data Found',
            'resultdata' => $provider->getModels(),
            'pagination' => [
                'perpage' => $provider->pagination->pageSize,
                'page' => $provider->pagination->page + 1,
                'totaldata' => $provider->totalCount,
            ],
        ];

        return response()->json($response);
    }


    /**
     * @SWG\Post(path="/{action}-storesubmission",
     *     tags={"submission"},
     *     summary="Create dokumen.",
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(in="path", name="action", type="string", enum={"create","draft-create"}),
     *     @SWG\Parameter(in="formData",name="files[]",type="array", @SWG\Items(type="string", format="binary")),
     *     @SWG\Parameter(in="formData",name="data",type="string", required=true,description="json encode data"),
     *     @SWG\Response(
     *         response = 200,
     *         description = "List dokumen",
     *         @SWG\Schema(type="boolean")
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function store(Request $request, $action)
    {
        if ($request->id) {
            return $this->update($request, $action);
        }
        $isDraft = ($action == 'draft-create');
        $user = auth()->user();
        $isPAK = $user->hasRole('pak');

        // mengambil data company dan cost center yang sedang login
        $dataUser = (new Query)
            ->select([
                'u.id', 'c.currency_id', 'c.id as company_id'
            ])
            ->from('users u')
            ->innerJoin('companies c', 'c.id=u.company_id')
            ->where(['u.id' => $user->id])
            ->one();
        if (!$isPAK && !$dataUser) {
            abort(401, 'Invalid user');
        }

        // Mengambil nilai JSON dari form-data dengan nama 'data'
        $data = $request->input('data');

        //  Decode JSON menjadi array asosiatif
        $decodedData = json_decode($data, true);

        $isHolding = false;
        $cc = null;
        if (!empty($decodedData['costcenter_id']) || $decodedData['costcenter_id'] == 0) {
            $cc = (new Query)
                ->select(['cc.*'])
                ->from('cost_centers cc')
                ->leftJoin('cost_center_user_inputs cu', 'cu.costcenter_id=cc.id')
                ->where(['cc.id' => $decodedData['costcenter_id'],])
                ->andWhere(['OR', ['cc.user_input_id' => $user->id], ['cu.user_id' => $user->id]])
                ->one();
        }

        if ($cc) {
            $isHolding = true;
        } elseif (!$isPAK) {
            abort(403, 'Forbidden');
        }

        // Validasi request
        $validatedData = Validator::make($decodedData, [
            'aktivitas' => 'required',
            'description' => 'required',
            'activity_background' => 'required',
            'dampak_aktivitas' => 'required',
            'resiko' => 'required',
            'dokumen_pendukung' => 'required',
            'ebitda' => 'numeric',
            'opex' => 'numeric',
            'tahun' => 'integer',
            'skenario' => 'required|array',
            'skenario.beban' => 'required|array',
            'skenario.beban.*.aktivitas_program' => 'required',
            'skenario.beban.*.cost_center' => 'string',
            'skenario.beban.*.cost_element' => 'string',
            'skenario.beban.*.bulan' => 'integer',
            'skenario.beban.*.akun' => 'string',
            'skenario.beban.*.nominal' => 'required|numeric',
        ]);

        // Mengunggah file pendukung (jika ada)
        $files = [];
        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('dokumen_pendukung');
                $files[] = [
                    'type' => 'dokumen_pendukung',
                    'url' => url('storage/' . $path),
                    'path' => 'storage/' . $path,
                ];
            }
        }
        if ($isHolding) {
            $status = $isDraft ? 0 : ($cc['user_atasan_id'] ? 30 : 40);
            $dataStore = [
                'company_id' => $dataUser['company_id'],
                'costcenter_id' => $cc['id'],
                'currency_id' => $dataUser['currency_id'],
                'status_id' => $status,
                'is_holding' => true,
                'aktifitas' => $decodedData['aktivitas'],
                'desc_aktifitas' => $decodedData['description'],
                'date' => Carbon::now(),
                'tahun' => $decodedData['tahun'] ?? date('Y'),
                'number' => $decodedData['number'],
                'latar_belakang' => $decodedData['activity_background'],
                'dampak' => $decodedData['dampak_aktivitas'],
                'resiko' => $decodedData['resiko'],
                'files' => json_encode($files),
                'created_by' => $user->id,
                'input_data' => $data,
                'user_atasan' => $cc['user_atasan_id'],
                'user_atasan2' => $cc['user_atasan2_id'],
                'user_dir_bidang' => $cc['user_dir_bidang_id'],
                'user_dirkeu' => $cc['user_dirkeu_id'],
            ];
        } else {
            $dirkeu_id = (new Query)
                ->select(['user_dirkeu_id'])
                ->from('cost_centers')
                ->where(['IS NOT', 'user_dirkeu_id', null])
                ->scalar();
            $dir_bidang = null;
            if (empty($decodedData['dir_bidang_id']) || $decodedData['dir_bidang_id'] == 0) {
                $dir_bidang = null;
                $status = ($isDraft ? 0 : 70);
            } else {
                $dir_bidang = (new Query)
                    ->select(['user_dir_bidang_id'])
                    ->from('cost_centers')
                    ->where(['id' => $decodedData['dir_bidang_id']])
                    ->scalar();
                $status = ($isDraft ? 0 : 50);
            }
            $dataStore = [
                'company_id' => $decodedData['company_id'] ?? null,
                'currency_id' => $dataUser['currency_id'],
                'status_id' => $status,
                'is_holding' => false,
                'aktifitas' => $decodedData['aktivitas'],
                'desc_aktifitas' => $decodedData['description'],
                'date' => Carbon::now(),
                'tahun' => $decodedData['tahun'] ?? date('Y'),
                'number' => $decodedData['number'],
                'latar_belakang' => $decodedData['activity_background'],
                'dampak' => $decodedData['dampak_aktivitas'],
                'resiko' => $decodedData['resiko'],
                'files' => json_encode($files),
                'created_by' => $user->id,
                'input_data' => $data,
                'user_dir_bidang' => $dir_bidang,
                'user_dirkeu' => $dirkeu_id,
            ];
            if ($dir_bidang) {
                $dataStore['dir_bidang_id'] = $decodedData['dir_bidang_id'];
            }
        }

        DB::transaction(function () use ($dataStore, $decodedData, $user, $status) {
            $next = (new Query)->from('trans_docs')->max('id') + 1;
            $dataStore['number'] = date('Y-m-d-') . str_pad($next, 6, '0', STR_PAD_LEFT);

            $amount = 0;
            foreach ($decodedData['skenario'] as $skenarioData) {
                foreach ($skenarioData['beban'] as $bebanData) {
                    $amount += $bebanData['nominal'];
                }
            }
            $dataStore['opex'] = $amount;
            $transDoc = TransDoc::create($dataStore);
            $transDocId = $transDoc->id;

            // Menyimpan data beban
            foreach ($decodedData['skenario'] as $skenarioData) {
                $skenario = Scenario::create([
                    'scenario_name' => $skenarioData['skenario_name'],
                    'trans_docs_id' => $transDocId,
                ]);

                foreach ($skenarioData['beban'] as $bebanData) {
                    $burden = Burden::create([
                        'aktifitas_program' => $bebanData['aktivitas_program'],
                        'cost_center' => $bebanData['cost_center'] ?? null,
                        'cost_element' => $bebanData['cost_element'] ?? null,
                        'akun' => $bebanData['akun'] ?? null,
                        // 'komponen_biaya' => $bebanData['komponen_biaya'] ?? null,
                        'bulan' => $bebanData['bulan'],
                        'nominal' => $bebanData['nominal'],
                        'scenario_id' => $skenario->id,
                    ]);
                }
            }

            // history
            $command = mDb()->createCommand();
            $command->insert('approval_histories', [
                'trans_doc_id' => $transDocId,
                'action' => 'create',
                'action_at' => Carbon::now(),
                'action_by' => $user->id,
                'doc_status' => $status,
                'prev_doc_status' => $status,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ])->execute();
        });


        // Response berhasil
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Submission Created'
        ], 201);
    }

    /**
     * @param int $cc_id
     * @param int $user_id
     * @return bool
     */
    protected function hasAccessCC($cc_id, $user_id)
    {
        return (new Query)
            ->select(['cc.*'])
            ->from('cost_centers cc')
            ->leftJoin('cost_center_user_inputs cu', 'cu.costcenter_id=cc.id')
            ->where([
                'OR',
                ['cu.user_id' => $user_id],
                ['cc.user_input_id' => $user_id],
            ])
            ->andWhere(['cu.costcenter_id' => $cc_id])
            ->exists();
    }

    public function update(Request $request, $action)
    {
        $isDraft = $action == 'draft-save';
        $user = auth()->user();
        $userId = $user->id;
        $transDoc = TransDoc::findOrFail($request->id);
        $status = $transDoc->status_id;
        $isPAK = $user->hasRole('pak');
        $isHolding = $transDoc->is_holding;


        $valid = (($status == 60 || $status == 61 || $status == 99) && $isPAK) ||
            (($status == 0 || $status == 1) && ($transDoc->created_by == $userId || $this->hasAccessCC($transDoc->costcenter_id, $userId)));
        if (!$valid) {
            abort(403, 'Forbiden');
            return;
        }

        // Mengambil nilai JSON dari form-data dengan nama 'data'
        $data = $request->input('data');
        $decodedData = json_decode($data, true);

        // Mengunggah file pendukung (jika ada)
        $files = [];
        if ($transDoc->files) {
            $files = json_decode($transDoc->files, true);
        }
        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                $path = $file->store('dokumen_pendukung');
                $files[] = [
                    'type' => 'dokumen_pendukung',
                    'url' => url('storage/' . $path),
                    'path' => 'storage/' . $path,
                ];
            }
        }

        switch ($status) {
            case 0:
            case 1:
                if ($isDraft) {
                    $newStatus = $status;
                } elseif ($transDoc['user_atasan']) {
                    $newStatus = 30;
                } elseif ($transDoc['user_atasan2']) {
                    $newStatus = 40;
                } elseif ($transDoc['user_dir_bidang']) {
                    $newStatus = 50;
                } else {
                    $newStatus = 70;
                }
                $dataUpdate = [
                    'status_id' => $newStatus,
                    'aktifitas' => $decodedData['aktivitas'],
                    'desc_aktifitas' => $decodedData['description'],
                    'latar_belakang' => $decodedData['activity_background'],
                    'ebitda' => $decodedData['ebitda'] ?? 0,
                    'dampak' => $decodedData['dampak_aktivitas'],
                    'resiko' => $decodedData['resiko'],
                    'files' => json_encode($files),
                    'input_data' => $data,
                ];
                if (!$isHolding) {
                    $dataUpdate = array_merge($dataUpdate, [
                        'is_kaitan_langsung' => $decodedData['is_kaitan_langsung'] ?? null,
                        'is_kaitan_tidak_langsung' => $decodedData['is_kaitan_tidak_langsung'] ?? null,
                        'point_perhatian_pak' => $decodedData['point_perhatian_pak'] ?? null,
                        'dampak_pak' => empty($decodedData['dampak_pak']) ? null : $decodedData['dampak_pak'],
                        'resiko_pak' => $decodedData['resiko_pak'] ?? null,
                        'rekomendasi_pak' => $decodedData['rekomendasi_pak'] ?? null,
                        'dampak_atas' => $decodedData['dampak_atas'] ?? null,
                        'dampak_tidak_sesuai_ajuan' => $decodedData['dampak_tidak_sesuai_ajuan'] ?? null,
                        'dampak_tidak_sesuai_realisasi' => $decodedData['dampak_tidak_sesuai_realisasi'] ?? null,
                        'dampak_sesuai_rekomendasi' => $decodedData['dampak_sesuai_rekomendasi'] ?? null,
                        'rekomendasi_tambah' => $decodedData['rekomendasi_tambah'] ?? null,
                    ]);
                }
                break;
            case 60:
            case 61:
            case 99:
                if ($isDraft) {
                    $newStatus = $status;
                } else {
                    $newStatus = ($status == 99 ? 100 : 70);
                }
                // hanya update data pak
                $dataUpdate = [
                    'status_id' => $newStatus,
                    'files' => json_encode($files),
                    'input_data' => $data,
                    'is_kaitan_langsung' => $decodedData['is_kaitan_langsung'] ?? null,
                    'is_kaitan_tidak_langsung' => $decodedData['is_kaitan_tidak_langsung'] ?? null,
                    'point_perhatian_pak' => $decodedData['point_perhatian_pak'] ?? null,
                    'dampak_pak' => empty($decodedData['dampak_pak']) ? null : $decodedData['dampak_pak'],
                    'resiko_pak' => $decodedData['resiko_pak'] ?? null,
                    'rekomendasi_pak' => $decodedData['rekomendasi_pak'] ?? null,
                    'dampak_atas' => $decodedData['dampak_atas'] ?? null,
                    'dampak_tidak_sesuai_ajuan' => $decodedData['dampak_tidak_sesuai_ajuan'] ?? null,
                    'dampak_tidak_sesuai_realisasi' => $decodedData['dampak_tidak_sesuai_realisasi'] ?? null,
                    'dampak_sesuai_rekomendasi' => $decodedData['dampak_sesuai_rekomendasi'] ?? null,
                    'rekomendasi_tambah' => $decodedData['rekomendasi_tambah'] ?? null,
                ];
                break;
        }

        // Mengambil ID data utama
        DB::transaction(function () use ($transDoc, $dataUpdate, $decodedData, $user, $status, $newStatus) {
            $transDocId = $transDoc->id;
            $amount = 0;
            foreach ($decodedData['skenario'] as $skenarioData) {
                foreach ($skenarioData['beban'] as $bebanData) {
                    $amount += $bebanData['nominal'];
                }
            }
            $dataUpdate['opex'] = $amount;

            $transDoc->update($dataUpdate);
            Scenario::where(['trans_docs_id' => $transDocId])->delete();

            foreach ($decodedData['skenario'] as $skenarioData) {
                $skenario = Scenario::create([
                    'scenario_name' => $skenarioData['skenario_name'],
                    'trans_docs_id' => $transDocId,
                ]);

                foreach ($skenarioData['beban'] as $bebanData) {
                    $burden = Burden::create([
                        'aktifitas_program' => $bebanData['aktivitas_program'],
                        'cost_center' => $bebanData['cost_center'] ?? null,
                        'cost_element' => $bebanData['cost_element'] ?? null,
                        'akun' => $bebanData['akun'] ?? null,
                        'komponen_biaya' => $bebanData['komponen_biaya'] ?? null,
                        'bulan' => $bebanData['bulan'],
                        'nominal' => $bebanData['nominal'],
                        'scenario_id' => $skenario->id,
                    ]);
                }
            }

            // history
            $command = mDb()->createCommand();
            $command->insert('approval_histories', [
                'trans_doc_id' => $transDocId,
                'action' => 'update',
                'action_at' => Carbon::now(),
                'action_by' => $user->id,
                'doc_status' => $newStatus,
                'prev_doc_status' => $status,
                'created_by' => $user->id,
                'updated_by' => $user->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ])->execute();
        });

        // Response berhasil
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Submission Created'
        ], 201);
    }

    public function report(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;

        $query = (new Query)
            ->select([
                'td.*', 'st.short',
                'st.short name_status', 'cc.description cost_center_name',
                'c.company_name', 'dir.description dir_bidang_name'
            ])
            ->from('trans_docs td')
            ->leftJoin('statuses st', 'st.status_code=td.status_id')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->orderBy(['td.number' => SORT_DESC]);

        $query->andFilterWhere([
            'td.date' => $request->query('date'),
            'td.tahun' => $request->query('tahun'),
            'td.created_by' => $request->query('created_by'),
            'td.costcenter_id' => $request->query('costcenter_id'),
            'td.status_id' => $request->query('status'),
            'td.company_id' => $request->query('company_id'),
        ]);

        $query->andFilterWhere(['ilike', 'td.number', $request->query('number')]);
        if ($q = $request->query('q')) {
            $query->andWhere([
                'OR',
                ['ilike', 'td.aktifitas', $q],
                ['ilike', 'td.desc_aktifitas', $q],
                ['ilike', 'td.latar_belakang', $q],
                ['ilike', 'td.dampak', $q],
                ['ilike', 'td.resiko', $q],
                ['ilike', 'td.resiko_pak', $q],
                ['ilike', 'td.rekomendasi_pak', $q],
            ]);
        }

        if (($holding = $request->query('holding'))) {
            $query->andWhere(['td.is_holding' => ($holding == 'uk')]);
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => request('page') - 1,
                'pageSize' => request('perpage'),
            ]
        ]);

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => $provider->totalCount . ' Data Found',
            'resultdata' => $provider->getModels(),
            'pagination' => [
                'perpage' => $provider->pagination->pageSize,
                'page' => $provider->pagination->page + 1,
                'totaldata' => $provider->totalCount,
            ],
        ];

        return response()->json($response);
    }

    protected function getHistories($id, $next = true)
    {
        $rows = (new Query)
            ->select([
                'ah.action', 'ah.action_at', 'ah.action_by',
                'ah.doc_status status_id', 'ah.prev_doc_status prev_status_id',
                'ah.note', 'ah.note_dirkeu',
                'u.name as action_by_name', 's1.short as status', 's2.short as prev_status'
            ])
            ->from('approval_histories ah')
            ->leftJoin('statuses s1', 's1.status_code=ah.doc_status')
            ->leftJoin('statuses s2', 's2.status_code=ah.prev_doc_status')
            ->leftJoin('users u', 'u.id=ah.action_by')
            ->where(['ah.trans_doc_id' => $id])
            ->orderBy(['ah.action_at' => SORT_DESC])
            ->all();
        if ($next) {
            $doc = (new Query)
                ->from('trans_docs')
                ->where(['id' => $id])
                ->one();
            $nextStatus = null;
            $user_id = null;
            switch ($doc['status_id']) {
                case '0':
                case '1':
                    if ($doc['user_atasan']) {
                        $nextStatus = 30;
                        $user_id = $doc['created_by'];
                    } elseif ($doc['user_atasan2']) {
                        $nextStatus = 40;
                        $user_id = $doc['created_by'];
                    } elseif ($doc['user_dir_bidang'] && $doc['user_dir_bidang'] != $doc['user_dirkeu']) {
                        $nextStatus = 50;
                        $user_id = $doc['created_by'];
                    } else {
                        $nextStatus = 60;
                        $user_id = $doc['created_by'];
                    }
                    break;
                case '30':
                    if ($doc['user_atasan2']) {
                        $nextStatus = 40;
                        $user_id = $doc['user_atasan'];
                    } elseif ($doc['user_dir_bidang'] && $doc['user_dir_bidang'] != $doc['user_dirkeu']) {
                        $nextStatus = 50;
                        $user_id = $doc['user_atasan'];
                    } else {
                        $nextStatus = 60;
                        $user_id = $doc['user_atasan'];
                    }
                    break;
                case '40':
                    if ($doc['user_dir_bidang'] && $doc['user_dir_bidang'] != $doc['user_dirkeu']) {
                        $nextStatus = 50;
                        $user_id = $doc['user_atasan2'];
                    } else {
                        $nextStatus = 60;
                        $user_id = $doc['user_atasan2'];
                    }
                    break;
                case '50':
                    $nextStatus = 60;
                    $user_id = $doc['user_dir_bidang'];
                    break;
                case '60':
                case '61':
                    $nextStatus = 70;
                    $user_id = 'pak';
                    break;
                case '70':
                    $nextStatus = 90;
                    $user_id = 'vp_pak';
                    break;
                case '90':
                case '99':
                    $nextStatus = 100;
                    $user_id = $doc['user_dirkeu'];
                    break;
            }
            if ($nextStatus) {
                $row = [
                    'action' => 'pending approve',
                    'status_id' => $nextStatus,
                    'status' => (new Query)
                        ->select(['short'])
                        ->from('statuses')
                        ->where(['status_code' => $nextStatus])
                        ->scalar(),
                    'action_by' => $user_id,
                    'action_by_name' => null,
                ];
                if ($user_id == 'pak') {
                    $row['action_by_name'] = 'PAK';
                } elseif ($user_id == 'vp_pak') {
                    $row['action_by_name'] = 'VP PAK';
                } elseif ($user_id) {
                    $row['action_by_name'] = (new Query)
                        ->select(['name'])
                        ->from('users')
                        ->where(['id' => $user_id])
                        ->scalar();
                }
                array_unshift($rows, $row);
            }
        }
        return $rows;
    }

    public function history()
    {
        $response = $this->getHistories(request('id'));
        return response()->json($response);
    }

    /**
     * @SWG\Post(path="/api/report-submissionView",
     *     tags={"submission"},
     *     summary="View dokumen.",
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(in="formData",name="id",type="integer", required=true,description="Dokumen id"),

     *     @SWG\Response(
     *         response = 200,
     *         description = "List dokumen",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *             @SWG\Property(property="resultdata", type="object",
     *                  @SWG\Property(property="statusdoc", type="string"),
     *                  @SWG\Property(property="transdoc", type="string"),
     *             ),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function reportView(Request $request, $id = null)
    {
        if ($id == null) {
            $id = $request->id;
        }
        $user = auth()->user();
        $transDoc = (new Query)
            ->select([
                'td.*', 'st.short',
                'st.short name_status', 'cc.description cost_center_name',
                'c.company_name', 'dir.description dir_bidang_name'
            ])
            ->from('trans_docs td')
            ->leftJoin('statuses st', 'st.status_code=td.status_id')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('companies c', 'c.id=td.company_id')
            ->leftJoin('cost_centers dir', 'dir.id=td.dir_bidang_id')
            ->where(['td.id' => $id])
            ->one();
        if (!$transDoc) {
            abort(404, 'Not Found');
            return;
        }

        $status = $transDoc['status_id'];

        $userId = $user->id;
        $isPAK = $user->hasRole('pak');
        $isVP_PAK = $user->hasRole('vp_pak');

        $isApprove = ($isVP_PAK && $status == 70) ||
            ($transDoc['user_atasan'] == $userId && $status == 30) ||
            ($transDoc['user_atasan2'] == $userId && $status == 40) ||
            ($transDoc['user_dir_bidang'] == $userId && $status == 50) ||
            ($transDoc['user_dir_bidang_v2'] == $userId && $status == 80) ||
            ($transDoc['user_dirkeu'] == $userId && $status == 90);

        $editable = (($userId == $transDoc['created_by'] || $this->hasAccessCC($transDoc['costcenter_id'], $userId)) && ($status == 0 || $status == 1)) ||
            ($isPAK && ($status == 60 || $status == 61 || $status == 99));
        // Mengecek status dokumen dan mengatur nilai tombol
        $statusDoc = [
            'button_approve' => $isApprove,
            'button_reject' => ($isApprove && $status != 70),
            'button_revisi' => $isApprove,
            'button_approvereason' => false, //($isApprove && $status == 90),
            'button_submit_edit' => $editable,
            'button_submit_save' => $editable,
            'button_save_draft' => $editable,
            'editable' => $editable,
            'show_rekomendasi' => $status >= 60,
        ];
        $histories = (new Query)
            ->select([
                'ah.action', 'ah.action_at', 'ah.action_by',
                'ah.doc_status status_id', 'ah.prev_doc_status prev_status_id',
                'ah.note', 'ah.note_dirkeu',
                'u.name as action_by_name', 's1.short as status', 's2.short as prev_status'
            ])
            ->from('approval_histories ah')
            ->leftJoin('statuses s1', 's1.status_code=ah.doc_status')
            ->leftJoin('statuses s2', 's2.status_code=ah.prev_doc_status')
            ->leftJoin('users u', 'u.id=ah.action_by')
            ->where(['ah.trans_doc_id' => $id])
            ->orderBy(['ah.action_at' => SORT_DESC])
            ->all();

        $data_support = [
            'trans_doc_id' => $transDoc['id'],
            'doc_desc' => $transDoc['doc_desc'],
            'files' => json_decode($transDoc['files'], true),
            'status_doc' => $status,
            'name_status' => $transDoc['name_status'],
        ];

        // Membuat array data TransDoc
        $data = array_merge($transDoc, [
            'aktivitas' => $transDoc['aktifitas'],
            'description' => $transDoc['desc_aktifitas'],
            'activity_background' => $transDoc['latar_belakang'],
            'dampak_aktivitas' => $transDoc['dampak'],
            'skenario' => []
        ]);
        $rows = (new Query)
            ->select([
                'sc.id', 'sc.scenario_name', 'b.*',
            ])
            ->from('scenarios sc')
            ->leftJoin('burdens b', 'b.scenario_id=sc.id')
            ->where(['sc.trans_docs_id' => $id])
            ->orderBy(['sc.id' => SORT_ASC, 'b.id' => SORT_ASC])
            ->all();
        $scenarios = [];
        foreach ($rows as $row) {
            $scId = $row['id'];
            if (!isset($scenarios[$scId])) {
                $scenarios[$scId] = [
                    'id' => $scId,
                    'skenario_name' => $row['scenario_name'],
                    'beban' => [],
                    'total_bulan' => [
                        1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
                        7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0,
                    ]
                ];
            }
            $scenarios[$scId]['total_bulan'][$row['bulan']] += $row['nominal'];
            $scenarios[$scId]['beban'][] = [
                'aktifitas_program' => $row['aktifitas_program'],
                'cost_center' => $row['cost_center'],
                'cost_element' => $row['cost_element'],
                'akun' => $row['akun'],
                'komponen_biaya' => $row['komponen_biaya'],
                'bulan' => $row['bulan'],
                'nominal' => $row['nominal']
            ];
        }
        $data['skenario'] = array_values($scenarios);

        $response = [
            'codestatus' => 'S',
            'message' => 'Success get data',
            'resultdata' => [
                'statusdoc' => $statusDoc,
                'transdoc' => $data,
                'data_support' => $data_support,
                'data' => json_decode($transDoc['input_data']),
                'histories' => $histories,
                'data_cost_center' => $transDoc['cost_center_name']
            ]
        ];

        return response()->json($response);
    }

    public function getDataLogin(Request $request)
    {
        $user = auth()->user();
        $data = (new Query)
            ->select(['c.company_name', 'cc.description', 'cc.id'])
            ->from('users u')
            ->innerJoin('companies c', 'c.id=u.company_id')
            ->innerJoin('cost_centers cc', 'cc.id=u.costcenter_id')
            ->where(['u.id' => $user->id])
            ->one();

        if (!$data) {
            abort(404, 'Data not found');
        }
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Success get data',
            'resultdata' => $data,
        ]);
    }

    public function getDataByMount(Request $request)
    {
        try {
            $user = auth()->user();

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data
            $data = TransDoc::join('scenarios', 'scenarios.trans_docs_id', '=', 'trans_docs.id')
                ->join('burdens', 'burdens.scenario_id', '=', 'scenarios.id')
                ->join('cost_centers', 'cost_centers.id', '=', 'trans_docs.costcenter_code')
                ->where('status_id', 90)
                ->where('cost_center_code', 'like',  '%' . $request->cost_center_code . '%')
                ->where('tahun', 'like',  '%' . $request->tahun . '%')
                ->where('bulan', 'like',  '%' . $request->bulan . '%')
                ->where('cost_element', 'like',  '%' . $request->gl_account . '%')
                ->select('cost_element', 'scenario_name', 'tahun', 'bulan', 'cost_center_code', 'description', DB::raw('SUM(burdens.nominal) as total_nominal'))
                ->groupBy('cost_element', 'scenario_name', 'tahun', 'bulan', 'cost_center_code', 'description')
                ->paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total() . ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        } catch (Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function getDataMobile()
    {
        try {
            // ambil data
            $data = TransDoc::join('unit_kerjas', 'unit_kerjas.id', '=', 'trans_docs.unit_kerja_id')
                ->join('companies', 'companies.id', '=', 'trans_docs.company_id')
                ->join('cost_centers', 'cost_centers.id', '=', 'trans_docs.costcenter_code')
                ->join('scenarios', 'scenarios.trans_docs_id', '=', 'trans_docs.id')
                ->join('burdens', 'burdens.scenario_id', '=', 'scenarios.id')
                ->select(
                    'unit_kerjas.id as unit_kerja_id',
                    'trans_docs.id as trans_docs_id',
                    'companies.id as id_company',
                    'cost_centers.id as id_cost_center',
                    'cost_centers.description',
                    'companies.company_name',
                    'trans_docs.status_id',
                    'trans_docs.number',
                    'trans_docs.desc_aktifitas',
                    'trans_docs.latar_belakang',
                    'trans_docs.dampak',
                    'trans_docs.resiko',
                    'unit_kerjas.name',
                    'trans_docs.aktifitas',
                    DB::raw('SUM(burdens.nominal) as total_nominal')
                )
                ->groupBy(
                    'unit_kerjas.id',
                    'trans_docs.id',
                    'companies.id',
                    'cost_centers.id',
                    'cost_centers.description',
                    'companies.company_name',
                    'unit_kerjas.name',
                    'trans_docs.aktifitas'
                )
                ->get();

            return ResponseFormatter::success($data, 'Success get data');
        } catch (Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function getAllUser(Request $request)
    {
        $user = auth()->user();

        $userData = (new Query)
            ->select(['id', 'name'])
            ->from('users')
            ->all();
        $response = [
            'codestatus' => 'S',
            'message' => 'Success get data',
            'resultdata' => $userData,
        ];

        return response()->json($response);
    }


    public function ReportSubmission(Request $request)
    {
        $user = auth()->user();

        // parameter search
        $id_user = $request->input('id_user');
        $status  = $request->input('status');
        $year    = $request->input('year');
        $cost_center = $request->input('cost_center');

        $query = (new Query)
            ->select(['td.*', 'st.short'])
            ->from('trans_docs td')
            ->leftJoin('cost_centers cc', 'cc.id=td.costcenter_id')
            ->leftJoin('statuses st', 'st.status_code=td.status_id');

        if (isset($id_user)) {
            $query->where(['td.created_by' => $id_user]);
        }
        if (isset($status)) {
            $query->andWhere(['st.short' => $status]);
        }

        if (isset($year)) {
            $query->andWhere(['td.tahun' => $year]);
        }
        if (isset($cost_center)) {
            $query->andWhere(['td.costcenter_id' => $cost_center]);
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => request('page') - 1,
                'pageSize' => request('perpage'),
            ]
        ]);
        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => $provider->totalCount . ' Data Found',
            'resultdata' => $provider->getModels(),
            'pagination' => [
                'perpage' => $provider->pagination->pageSize,
                'page' => $provider->pagination->page + 1,
                'totaldata' => $provider->totalCount,
            ],
        ];

        return response()->json($response);
    }


    public function ReportSubmissionView(Request $request)
    {
        $user = auth()->user();

        // ambil id dari request
        $transDocId = $request->id;
        $transDoc = TransDoc::findOrFail($transDocId);
        $status = $transDoc->status_id;
        $valuestatusCC = (new Query)
            ->select('short')
            ->from('statuses')
            ->where(['status_code' => $status])
            ->scalar();
        $costcenter_id = $transDoc->costcenter_id;
        $valueCostCenter = (new Query)
            ->select('description')
            ->from('cost_centers')
            ->where(['id' => $costcenter_id])
            ->scalar();

        // $userId = $user->id;
        // $role = $user->hasRole('pak');
        // $valid = ($userId == $transDoc->created_by) || $isPAK;

        // if (!$valid) {
        //     abort(403, 'Forbiden');
        //     return;
        // }

        $editable = ($status == 0 || $status == 1 || $status == 60 || $status == 61 || $status == 99);
        // Mengecek status dokumen dan mengatur nilai tombol
        $statusDoc = [
            'button_approve' => !$editable,
            'button_reject' => !$editable,
            'button_revisi' => !$editable,
            'button_approvereason' => $status == 90,
            'button_submit_edit' => $editable,
            'button_submit_save' => $editable,
            'editable' => $editable,
            // 'role' => $isPAK,
            'show_rekomendasi' => $status >= 60,
        ];

        $histories = (new Query)
            ->select([
                'ah.action', 'ah.action_at', 'ah.action_by',
                'ah.doc_status status_id', 'ah.prev_doc_status prev_status_id',
                'ah.note', 'ah.note_dirkeu',
                'u.name as action_by_name', 's1.short as status', 's2.short as prev_status'
            ])
            ->from('approval_histories ah')
            ->leftJoin('statuses s1', 's1.status_code=ah.doc_status')
            ->leftJoin('statuses s2', 's2.status_code=ah.prev_doc_status')
            ->leftJoin('users u', 'u.id=ah.action_by')
            ->where(['ah.trans_doc_id' => $transDoc->id])
            ->orderBy(['ah.action_at' => SORT_DESC])
            ->all();
        $data_support = [
            'trans_doc_id' => $transDoc->id,
            'doc_desc' => $transDoc->doc_desc,
            'files' => json_decode($transDoc->files, true),
            'status_doc' => $status,
            'name_status' => $valuestatusCC,
            'histories' => $histories,
        ];
        // Menggabungkan status dokumen dan data TransDoc dalam satu array response
        $response = [
            'codestatus' => 'S',
            'message' => 'Success get data',
            'resultdata' => [
                'statusdoc' => $statusDoc,
                'data_support' => $data_support,
                'data' => json_decode($transDoc->input_data),
                'data_cost_center' => $valueCostCenter
            ]
        ];
        return response()->json($response);
    }

    public function updateSubmission(Request $request)
    {
        $data = $request->all();

        $update = TransDoc::where('id', $data['id'])->update($data);

        // Response berhasil
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Submission Updated'
        ], 201);
    }
}
