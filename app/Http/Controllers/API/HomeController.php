<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Models\User;
use App\Models\TransDoc;

class HomeController extends Controller
{
    public function index()
    {
        try{
            return ResponseFormatter::success("Test ini API halaman Home", 'Get Home Screen');
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function getData(Request $request)
    {

        return "asdasdasd";
        
        try{
            // ambil token pada header
            $token = $request->header('Token');

           

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Total User
            $userTotal = User::all();
            $countUser = count($userTotal);

            // Total Ajuan
            $totalAjuan = TransDoc::join('scenarios', 'scenarios.trans_docs_id', '=', 'trans_docs.id')
                        ->join('burdens', 'burdens.scenario_id', '=', 'scenarios.id')
                        ->where('status_id', '=', 30)
                        ->sum('nominal');

            // Total Approve
            $totalApprove = TransDoc::join('scenarios', 'scenarios.trans_docs_id', '=', 'trans_docs.id')
                        ->join('burdens', 'burdens.scenario_id', '=', 'scenarios.id')
                        ->where('status_id', '=', 100)
                        ->sum('nominal');

            // Kumpulkan Data
            $responseSummary = [
                "total_user" => $countUser,
                "total_ajuan" => $totalAjuan,
                "total_approve" => $totalApprove
            ];

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => 'Get Summary',
                'resultdata' => $responseSummary,
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }
}
