<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Status;
use App\Models\User;
use App\Helpers\ResponseFormatter;

class StatusController extends Controller
{
    public function getStatus(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data dari sumber data Anda
            $data = Status::paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }
}
