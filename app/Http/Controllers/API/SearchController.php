<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Status;
use App\Models\User;
use App\Helpers\ResponseFormatter;
use yii\db\Query;

class SearchController extends Controller
{
    public function status()
    {
        $rows = (new Query)
            ->select([
                'id' => 'status_code',
                'code' => 'status_code',
                'name' => 'short',
            ])
            ->from('statuses')
            ->orderBy(['status_code' => SORT_ASC])
            ->all();
        return response()->json($rows);
    }



    /**
     * @SWG\Post(path="/api/user-costcenter",
     *     tags={"user_cost_center"},
     *     summary="View dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="array", 
     *              @SWG\Items(type="object", 
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="cost_center_code", type="string"),
     *                  @SWG\Property(property="description", type="string"),
     *                  @SWG\Property(property="cost_center_category", type="string"),
     *                  @SWG\Property(property="company_id", type="integer"),
     *                  @SWG\Property(property="currency_id", type="integer"),
     *                  @SWG\Property(property="user_atasan_id", type="integer"),
     *                  @SWG\Property(property="user_atasan2_id", type="integer"),
     *                  @SWG\Property(property="user_dir_bidang_id", type="integer"),
     *                  @SWG\Property(property="user_dirkeu_id", type="integer"),
     *              ),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */
    
    public function userCostCenter()
    {
        $user = auth()->user();
        $rows = (new Query)
            ->select(["cc.*, concat(cc.cost_center_code, ' - ', cc.description) as fullname"])
            ->from('cost_centers cc')
            ->leftJoin('cost_center_user_inputs cu', 'cu.costcenter_id=cc.id')
            ->where(['OR', 
                ['cu.user_id' => $user->id],
                ['cc.user_input_id' => $user->id],
            ])
            ->orderBy(['cc.hirarchy' => SORT_ASC])
            ->all();

        return response()->json($rows);
    }

    public function userCostCenterAll()
    {
        $rows = (new Query)
            ->select(['*'])
            ->from('cost_centers')
            ->orderBy(['hirarchy' => SORT_ASC])
            ->all();

        return response()->json($rows);
    }


    public function dirBidang()
    {
        $rows = (new Query)
            ->select(['*'])
            ->from('cost_centers')
            ->where(['length(hirarchy)' => 3])
            ->orderBy(['hirarchy' => SORT_ASC])
            ->all();

        return response()->json($rows);
    }
}
