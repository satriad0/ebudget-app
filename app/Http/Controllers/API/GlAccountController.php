<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlAccount;
use App\Models\User;
use DB;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;

class GlAccountController extends Controller
{
    public function getGlAccount(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data dari sumber data Anda
            $data = GlAccount::paginate($intPerPgae,['*', DB::RAW("CONCAT(gl_account, ' - ', description) as fullname")], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function create(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            $body = $request->all();

            GlAccount::create([
                "gl_account" => $body['gl_account'],
                "description" => $body['description'],
                "account_type" => $body['account_type'],
                "cf_acc" => $body['cf_acc'],
                "cf_tb" => $body['cf_tb'],
                "contra_acc1" => $body['contra_acc1'],
                "dimlist" => $body['dimlist'],
                "fs_structure" => $body['fs_structure'],
                "group" =>  $body['group'],
                "rate_type" => $body['rate_type'],
                "type_elim" => $body['type_elim'],
                "parenth1" => $body['parenth1'],
                "gl_id" => $body['gl_id'],
                "last_update" => $body['last_update'],
                "cash_flow" => $body['cash_flow'],
                "group_cycle" => $body['group_cycle'],
                "structure_costing" => $body['structure_costing'],
                "group_hrgl_" => $body['group_hrgl_'],
                "created_by" => $user->id,
                "updated_by" => $user->id,
            ]);

            return ResponseFormatter::success([], 'Created Gl Account', 201);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function update(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }


            $id = $request->gl_account_id;
            $body = $request->all();
            $body = request()->except(['gl_account_id']);
            $glAccount = GlAccount::where('id', $id);
            $glAccount->update($body);

            return ResponseFormatter::success([], 'Updated Gl Account', 200);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function delete(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            $id = $request->gl_account_id;
            GlAccount::where('id', $id)->delete();

            return ResponseFormatter::success([], 'Deleted Gl Account', 200);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function search(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            $body = $request->search;

            $data = GlAccount::where('gl_account', 'like', '%'.$body.'%')
                    ->orWhere('description', 'like', '%'.strtoupper($body).'%')
                    ->paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }
}
