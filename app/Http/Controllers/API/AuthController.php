<?php

namespace App\Http\Controllers\API;

use App\Helpers\JWT;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * @SWG\Post(path="/api/request-token",
     *     tags={"auth"},
     *     summary="Request auth token.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(
     *             required={"username"},
     *             @SWG\Property(property="username", type="string"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Jwt",
     *         @SWG\Schema(
     *              @SWG\Property(property="token", type="string"),
     *         )
     *     )
     * )
     */
    public function requestToken(Request $request)
    {
        $user = User::where('username', $request->post('username'))->first();
        if (!$user) {
            abort(401, 'Invalid user email');
        }
        $payload = ['sub' => $user->id];
        return response()->json([
            'token' => JWT::encode($payload),
        ]);
    }

    public function getAllUser(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data dari sumber data Anda
            $data = User::paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }
}
