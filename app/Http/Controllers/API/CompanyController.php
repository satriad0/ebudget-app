<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\User;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function getCompany(Request $request)
    {
        try{
            // ambil token pada header
            $user = auth()->user();

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data dari sumber data Anda
            $data = Company::paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function create(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            $body = $request->all();

            $validate = Validator::make($body, [
                'company_code' => 'required',
                'company_name' => 'required',
                'address' => 'required',
                'country' => 'required',
                'currency_id' => 'required',
                'hirarchy' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'FM_Area' => 'required',
            ]);

            if ($validate->fails()) {
                $response = [
                    'errors' => $validate->errors()
                ];
                return ResponseFormatter::error($response, 'Bad Request', 400);
            }

            Company::create([
                'company_code' => $body['company_code'],
                'company_name' => $body['company_name'],
                'address' => $body['address'],
                'country' => $body['country'],
                'currency_id' => $body['currency_id'],
                'hirarchy' => $body['hirarchy'],
                'start_date' => $body['start_date'],
                'end_date' => $body['end_date'],
                'FM_Area' => $body['FM_Area'],
                'created_by' => $user->id,
            ]);

            return ResponseFormatter::success([], 'Created Company', 201);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function update(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            $id = $request->company_id;
            $body = $request->all();
            $body = request()->except(['company_id']);
            $company = Company::where('id', $id);
            $company->update($body);

            return ResponseFormatter::success([], 'Updated Company', 200);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function delete(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            $id = $request->company_id;
            Company::where('id', $id)->delete();

            return ResponseFormatter::success([], 'Deleted Company', 200);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }

    public function search(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            $body = $request->search;

            $data = Company::where('company_name', 'like', '%'.$body.'%')
                    ->orWhere('company_code', 'like', '%'.$body.'%')
                    ->paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }
}
