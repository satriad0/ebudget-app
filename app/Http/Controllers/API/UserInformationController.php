<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class UserInformationController extends Controller
{
    private $_host = "10.60.12.120";
    private $_postfix = "@smig.corp";
    private $_ret_error = array("loginreturn" => false, "msg" => "Gagal");
    private $_versionCodeAllowed = 7;
 
    public function getDataUser(Request $request)
    {
        try{
            // ambil token pada header
            // $token = $request->header('Token');

            // // cek token apakah valid
            // $user = User::where('remember_token', $token)->first();
            // if(!$user){
            //     return ResponseFormatter::error([], 'Unauthorized', 401);
            // }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data dari sumber data Anda
            $data = User::join('user_information', 'users.id', '=', 'user_information.user_id')->paginate($intPerPgae, ['*'], 'page', $intPage);
            // Buat format respons
            $response = [
                'codestatus' => '200',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }
    
     /**
     *displays company data.
     */
    public function getCompany(Request $request)
    {
        $company_name = $request->input('company_name');

        try{
            // // ambil token pada header
            // $token = $request->header('Token');

            // // cek token apakah valid
            // $user = User::where('remember_token', $token)->first();
            // if(!$user){
            //     return ResponseFormatter::error([], 'Unauthorized', 401);
            // }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            

            // Ambil data dari sumber data Anda
            $data = Company::where('company_name', 'like', '%'.$company_name.'%')->paginate($intPerPgae, ['*'], 'page', $intPage);


            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }


    }

     /**
     *displays Cost Center data.
     */
    public function getCostCenter(Request $request)
    {
        $description = $request->input('description');
        try{
            // // ambil token pada header
            // $token = $request->header('Token');

            // // cek token apakah valid
            // $user = User::where('remember_token', $token)->first();
            // if(!$user){
            //     return ResponseFormatter::error([], 'Unauthorized', 401);
            // }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            // Ambil data dari sumber data Anda
            $data = CostCenter::where('description', 'like', '%'.$description.'%')->paginate($intPerPgae, ['*'], 'page', $intPage);    
            // Buat format respons
            $response = [
                'codestatus' => '200',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }


    public function syncSSO(Request $request)
    {
        try {
            // get username and password
            $email = request('email');
            $password = request('password');

            //Login SSO server prod
            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL,"http://10.15.5.123:8000/api/login");
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS,"username=$email&password=$password&token=9fd977dad8b0e2231b7a2112faa889df");
            // curl_setopt($ch, CURLOPT_HTTPHEADER, array('token:$2y$10$3DDqyL./M7Qn4h426rnOAux3H20.VWXE2sqO83tk6n24QDtswGwF.')); // when akses using token

            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // $server_output = curl_exec($ch);
            // if (curl_errno($ch)) {
            //     $error_msg = curl_error($ch);
            // }
            // if (isset($error_msg)) {
            //     echo $error_msg;
            // }
            // curl_close ($ch);
            // $server_output = json_decode($server_output,true);
            // dd($server_output);
            //server dev
            // if ($email != '' && $password == 'password') {
            if ($email != '') {
                $server_output = true;
            }else{
                $server_output = false;
            }

            if ($server_output == true) {
                return response()->json([
                    'codestatus' => true,
                    'message' => "Sync SSO Success",
                ]);
            }else{
                return response()->json([
                    'codestatus' => False,
                    'message' => "Sync SSO Failed",
                ]);
            }
           
            //end login SSO

        } catch (Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }


    public function syncLDAP(Request $request)
    {
        
            // get username and password
            $username = request('username');
            $password = request('password');
            //sync LDAP
            // if($username == 'user.test' && $password == 's3men1ndo'){
            // if (config('app.env') == 'local') {
                if($username == 'user.test'){
                    $ldap['bind'] = true;
                    // return true;
                }else{
                    $ldap['bind'] = false;
                }
            // }else{
            
            // $ldap['user'] = $username . $this->_postfix; // di tambahn @semenindonesia
            // if (empty($password)) {
            //     $ldap['pass'] = $password . 'lksdaldvlj8'; //biar error
            // } else {
            //     $ldap['pass'] = $password;
            // }   
            
            
            // $ldap['host'] = $this->_host;
            // $ldap['port'] = 389;
            // $ldap['conn'] = ldap_connect($ldap['host'], $ldap['port'])
            //         or die("Could not conenct to {$ldap['host']}");
            // ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
            // $ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
            // ldap_close($ldap['conn']);
            // }

            if ($ldap['bind'] == true) {
                return response()->json([
                    'codestatus' => true,
                    'message' => "Sync LDAP Success",
                ]);
            }else{
                return response()->json([
                    'codestatus' => False,
                    'message' => "Sync LDAP Failed",
                ]);
            }
           
            //end sync LDAP

        
    }



    
}
