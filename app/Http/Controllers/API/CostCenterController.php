<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CostCenterUserInput;
use Illuminate\Http\Request;
use App\Models\CostCenter;
use App\Models\User;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class CostCenterController extends Controller
{

    /**
     * @SWG\Post(path="/api/cost-center",
     *     tags={"cost_center"},
     *     summary="List dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="query", name="q", type="string"),
     *     @SWG\Parameter(in="query", name="id", type="integer"),
     *     @SWG\Parameter(in="query", name="cost_center_code", type="string"),
     *     @SWG\Parameter(in="query", name="description", type="string"),
     *     @SWG\Parameter(in="query", name="hirarchy", type="string"),
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="page", type="integer"),
     *             @SWG\Property(property="perpage", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "List dokumen",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *             @SWG\Property(property="resultdata", type="array", @SWG\Items(type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="cost_center_code", type="string"),
     *                  @SWG\Property(property="description", type="string"),
     *                  @SWG\Property(property="cost_center_category", type="string"),
     *                  @SWG\Property(property="company_id", type="integer"),
     *                  @SWG\Property(property="currency_id", type="integer"),
     *                  @SWG\Property(property="user_atasan_id", type="integer"),
     *                  @SWG\Property(property="user_atasan2_id", type="integer"),
     *                  @SWG\Property(property="user_dir_bidang_id", type="integer"),
     *                  @SWG\Property(property="user_dirkeu_id", type="integer"),
     *             )),
     *             @SWG\Property(property="pagination", ref="#/definitions/Pagination"),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function index(Request $request)
    {
        $query = (new Query)
            ->select(['cc.*'])
            ->from('cost_centers cc');

        $query->andFilterWhere([
            'cc.id' => $request->query('id'),
        ]);

        $query->andFilterWhere(['ilike', 'cc.cost_center_code', $request->query('cost_center_code')])
            ->andFilterWhere(['ilike', 'cc.description', $request->query('description')])
            ->andFilterWhere(['ilike', 'cc.hirarchy', $request->query('hirarchy')]);

        if ($q = $request->query('q')) {
            $query->andWhere([
                'OR',
                ['ilike', 'cc.cost_center_code', $q],
                ['ilike', 'cc.description', $q],
                ['ilike', 'cc.hirarchy', $q],
            ]);
        }
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => intval(request('page')) - 1,
                'pageSize' => request('perpage'),
            ]
        ]);

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => $provider->totalCount . ' Data Found',
            'resultdata' => $provider->getModels(),
            'pagination' => [
                'perpage' => $provider->pagination->pageSize,
                'page' => $provider->pagination->page + 1,
                'totaldata' => $provider->totalCount,
            ],
        ];

        return response()->json($response);
    }

    /**
     * @SWG\Post(path="/api/cost-center-view",
     *     tags={"cost_center"},
     *     summary="View dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *             @SWG\Property(property="resultdata", type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="cost_center_code", type="string"),
     *                  @SWG\Property(property="description", type="string"),
     *                  @SWG\Property(property="cost_center_category", type="string"),
     *                  @SWG\Property(property="company_id", type="integer"),
     *                  @SWG\Property(property="currency_id", type="integer"),
     *                  @SWG\Property(property="user_atasan_id", type="integer"),
     *                  @SWG\Property(property="user_atasan2_id", type="integer"),
     *                  @SWG\Property(property="user_dir_bidang_id", type="integer"),
     *                  @SWG\Property(property="user_dirkeu_id", type="integer"),
     *             ),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */
    public function view(Request $request)
    {
        $cc = (new Query)
            ->select(['cc.*'])
            ->from('cost_centers cc')
            ->where(['cc.id' => $request->id])
            ->one();

        if (!$cc) {
            abort(404);
        }

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => 'Data Found',
            'resultdata' => $cc,
        ];

        return response()->json($response);
    }

    /**
     * @SWG\Post(path="/api/cost-center-create",
     *     tags={"cost_center"},
     *     summary="Create dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="cost_center_code", type="string"),
     *             @SWG\Property(property="description", type="string"),
     *             @SWG\Property(property="cost_center_category", type="string"),
     *             @SWG\Property(property="company_id", type="integer"),
     *             @SWG\Property(property="currency_id", type="integer"),
     *             @SWG\Property(property="user_atasan_id", type="integer"),
     *             @SWG\Property(property="user_atasan2_id", type="integer"),
     *             @SWG\Property(property="user_dir_bidang_id", type="integer"),
     *             @SWG\Property(property="user_dirkeu_id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */    
    public function store(Request $request)
    {
        $user = auth()->user();
        $body = $request->all();

        $validate = Validator::make($body, [
            'cost_center_code' => 'required',
            'description' => 'required',
            'hirarchy' => 'required',
            'cost_center_category' => 'required',
            'company_id' => 'required',
            'currency_id' => 'required',
            'profit_center' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'cost_category_desc' => 'required',
        ]);

        if ($validate->fails()) {
            $response = [
                'errors' => $validate->errors()
            ];
            return response()->json($response, 422);
        }

        CostCenter::create([
            'cost_center_code' => $body['cost_center_code'],
            'description' => $body['description'],
            'hirarchy' => $body['hirarchy'],
            'cost_center_category' => $body['cost_center_category'],
            'company_id' => $body['company_id'],
            'currency_id' => $body['currency_id'],
            'profit_center' => $body['profit_center'],
            // 'cost_center_category2' => $body['cost_center_category2'],
            'start_date' => $body['start_date'],
            'end_date' => $body['end_date'],
            'cost_category_desc' => $body['cost_category_desc'],
            'created_by' => $user->id,
        ]);
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Cost Center Created'
        ], 201);
    }

    
    /**
     * @SWG\Post(path="/api/cost-center-update",
     *     tags={"cost_center"},
     *     summary="Update dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="id", type="integer"),
     *             @SWG\Property(property="cost_center_code", type="string"),
     *             @SWG\Property(property="description", type="string"),
     *             @SWG\Property(property="cost_center_category", type="string"),
     *             @SWG\Property(property="company_id", type="integer"),
     *             @SWG\Property(property="currency_id", type="integer"),
     *             @SWG\Property(property="user_atasan_id", type="integer"),
     *             @SWG\Property(property="user_atasan2_id", type="integer"),
     *             @SWG\Property(property="user_dir_bidang_id", type="integer"),
     *             @SWG\Property(property="user_dirkeu_id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */    
    public function update(Request $request)
    {
        $id = $request->id;
        $model = CostCenter::where('id', $id);
        $body = $request->except(['id']);
        $model->update($body);
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Cost Center Updated'
        ], 200);
    }

    
    /**
     * @SWG\Post(path="/api/cost-center-delete",
     *     tags={"cost_center"},
     *     summary="Update dokumen.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */    
    public function delete(Request $request)
    {
        $id = $request->id;
        $model = CostCenter::where('id', $id);
        $model->delete();
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Cost Center Deleted'
        ], 200);
    }

    /**
     * @SWG\Post(path="/api/cost-center/users",
     *     tags={"cost_center"},
     *     summary="List input user.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *             @SWG\Property(property="resultdata", type="array", @SWG\Items(type="object",
     *                  @SWG\Property(property="user_id", type="integer"),
     *                  @SWG\Property(property="user_name", type="string"),
     *             )),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */    
    public function users(Request $request)
    {
        $rows = (new Query)
            ->select(['ci.user_id', 'u.name user_name'])
            ->from('cost_center_user_inputs ci')
            ->innerJoin('users u', 'u.id=ci.user_id')
            ->where(['ci.costcenter_id' => $request->get('id')])
            ->orderBy(['ci.user_id' => SORT_ASC])
            ->all();

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => 'Data Found',
            'resultdata' => $rows,
        ];

        return response()->json($response);
    }

    
    /**
     * @SWG\Post(path="/api/cost-center/add-users",
     *     tags={"cost_center"},
     *     summary="Add input user.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="id", type="integer"),
     *             @SWG\Property(property="user_id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */    
    public function addUsers(Request $request)
    {
        $id = $request->id;
        $cc = (new Query)
            ->select(['cc.*'])
            ->from('cost_centers cc')
            ->where(['cc.id' => $id])
            ->one();

        if (!$cc) {
            abort(404);
        }

        $user_id = $request->user_id;
        $exists = (new Query)
            ->select(['ci.*'])
            ->from('cost_center_user_inputs ci')
            ->where(['ci.costcenter_id' => $id, 'ci.user_id' => $user_id])
            ->exists();
        if ($exists) {
            return response()->json([
                'codestatus' => 'E',
                'message' => 'Data duplicated',
            ]);
        }
        CostCenterUserInput::create([
            'costcenter_id' => $id,
            'user_id' => $user_id,
        ]);

        // Buat format respons
        $response = [
            'codestatus' => 'S',
            'message' => 'Data created',
        ];

        return response()->json($response);
    }

    /**
     * @SWG\Post(path="/api/cost-center/delete-users",
     *     tags={"cost_center"},
     *     summary="Delete input user.",
     *     consumes={"application/json"},
     *     @SWG\Parameter(in="body", name="body",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="id", type="integer"),
     *             @SWG\Property(property="user_id", type="integer"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Response",
     *         @SWG\Schema(type="object",
     *             @SWG\Property(property="codestatus", type="string", example="S"),
     *             @SWG\Property(property="message", type="string"),
     *         )
     *     ),
     *     security={{"token": {}}},
     * )
     */    


    public function search(Request $request)
    {
        try{
            // ambil token pada header
            $token = $request->header('Token');

            // cek token apakah valid
            $user = User::where('remember_token', $token)->first();
            if(!$user){
                return ResponseFormatter::error([], 'Unauthorized', 401);
            }

            // Ambil nomor halaman dari permintaan
            $perPage = request('perpage');
            $page = request('page');

            // ubah request kedalam integer
            $intPerPgae = (int)$perPage;
            $intPage = (int)$page;

            $body = $request->search;

            $data = CostCenter::where('description', 'like', '%'.$body.'%')
                    ->orWhere('cost_center_code', 'like', '%'.$body.'%')
                    ->paginate($intPerPgae, ['*'], 'page', $intPage);

            // Buat format respons
            $response = [
                'codestatus' => 'S',
                'message' => $data->total(). ', Data Found',
                'pagination' => [
                    'perpage' => $data->perPage(),
                    'page' => $data->currentPage(),
                    'totaldata' => $data->total(),
                ],
                'resultdata' => $data->items(),
            ];

            return response()->json($response);
        }catch(Exception $e) {
            $response = [
                'errors' => $e->getMessage(),
            ];
            return ResponseFormatter::error($response, 'Something went wrong', 500);
        }
    }


    public function deleteUsers(Request $request)
    {
        $id = $request->id;
        $user_id = $request->user_id;
        $model = CostCenterUserInput::where('costcenter_id', $id)
            ->where('user_id', $user_id);
        $model->delete();
        return response()->json([
            'codestatus' => 'S',
            'message' => 'Cost Center User Deleted'
        ], 200);
    }
}
