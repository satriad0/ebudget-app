<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Status;
use App\Models\User;
use App\Helpers\ResponseFormatter;
use yii\db\Query;

class DashboardController extends Controller
{
    public function userCount()
    {
        $count = (new Query)
            ->from('users')
            ->count();
        return response()->json($count);
    }

    public function docCount($type)
    {
        $query = (new Query)
            ->from('trans_docs');
        switch ($type) {
            case 'inprogress':
                $query->andWhere(['<', 'status_id', 100]);
                break;
            case 'approve':
                $query->andWhere(['=', 'status_id', 100]);
                break;
            case 'active':
                $query->andWhere(['<=', 'status_id', 100]);
                break;
            case 'reject':
                $query->andWhere(['=', 'status_id', 110]);
                break;
        }

        if (($holding = request('holding'))) {
            $query->andWhere(['is_holding' => ($holding == 'uk')]);
        }

        return response()->json($query->count());
    }

    public function docAmount($type)
    {
        $query = (new Query)
            ->from('trans_docs');
        switch ($type) {
            case 'inprogress':
                $query->andWhere(['<', 'status_id', 100]);
                break;
            case 'approve':
                $query->andWhere(['=', 'status_id', 100]);
                break;
            case 'active':
                $query->andWhere(['<=', 'status_id', 100]);
                break;
            case 'reject':
                $query->andWhere(['=', 'status_id', 110]);
                break;
        }

        if (($holding = request('holding'))) {
            $query->andWhere(['is_holding' => ($holding == 'uk')]);
        }

        return response()->json($query->sum('opex'));
    }

    public function all()
    {
        $result = [
            'count_user' => (new Query)
                ->from('users')
                ->count(),
            'count_inprogres_docs' => 0,
            'count_approve_docs' => 0,
            'count_active_docs' => 0,
            'count_reject_docs' => 0,
            'count_all_docs' => 0,
            'amount_inprogres_docs' => 0,
            'amount_approve_docs' => 0,
            'amount_active_docs' => 0,
            'amount_reject_docs' => 0,
            'amount_all_docs' => 0,
        ];

        $rows = (new Query)
            ->select([
                'status_id',
                'jml' => 'COUNT(*)',
                'amount' => 'SUM(opex)'
            ])
            ->from('trans_docs')
            ->groupBy(['status_id'])
            ->all();
        foreach ($rows as $row) {
            if ($row['status_id'] < 100) {
                $result['count_inprogres_docs'] += $row['jml'];
                $result['amount_inprogres_docs'] += $row['amount'];
                $result['count_active_docs'] += $row['jml'];
                $result['amount_active_docs'] += $row['amount'];
                $result['count_all_docs'] += $row['jml'];
                $result['amount_all_docs'] += $row['amount'];
            } elseif ($row['status_id'] == 100) {
                $result['count_approve_docs'] += $row['jml'];
                $result['amount_approve_docs'] += $row['amount'];
                $result['count_active_docs'] += $row['jml'];
                $result['amount_active_docs'] += $row['amount'];
                $result['count_all_docs'] += $row['jml'];
                $result['amount_all_docs'] += $row['amount'];
            } elseif ($row['status_id'] == 110) {
                $result['count_reject_docs'] += $row['jml'];
                $result['amount_reject_docs'] += $row['amount'];
                $result['count_all_docs'] += $row['jml'];
                $result['amount_all_docs'] += $row['amount'];
            }
        }

        return response()->json($result);
    }
}
