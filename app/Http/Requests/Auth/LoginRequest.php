<?php

namespace App\Http\Requests\Auth;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class LoginRequest extends FormRequest
{
    
    private $_host = "10.60.12.120";
    private $_postfix = "@smig.corp";
    private $_ret_error = array("loginreturn" => false, "msg" => "Gagal");
    private $_versionCodeAllowed = 7;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ];
    }

    /**
     * Attempt to authenticate the request's credentials.
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticate()
    {
        $email = $this->only('email')['email'];
        $password = $this->only('password')['password'];

        /* cek apakah dia ada unsur super admin */
        $mailuser = explode('@',$email);
        $checkSuperUser = false;
        
        if($mailuser[0] == "superadmin" ||  $mailuser[0] == "joko.susilo")
        {
            $checkSuperUser = true;
        }
        //dd($checkSuperUser);
        // $filter = preg_quote('superadmin', '~');
        // $result = preg_grep('~' . $filter . '~', $mail);
        // $checkSuperUser  = count($result) >= 1 ? true : false;

        if ($checkSuperUser == true) {
            if (! Auth::attempt(['email' => $email, 'password' => $password], $this->boolean('remember'))) {
                RateLimiter::hit($this->throttleKey());

                 throw ValidationException::withMessages([
                    'email' => trans('auth.failed'),
                ]);
            }
        } else if (env('LOGIN_SSO')) {
            $cekSSO = $this->checkSSO($email,$password);
            // dd($cekSSO);
            $is_usersso = (isset($cekSSO)) ? $cekSSO['success'] : false;
            if ($is_usersso) {
                // pengecekan SSO
                    $this->ensureIsNotRateLimited();
                    //if (! Auth::attempt($this->only('email', 'password'), $this->boolean('remember'))) {
                    if (! Auth::attempt(['email' => $email, 'password' => 'password'], $this->boolean('remember'))) {
                        RateLimiter::hit($this->throttleKey());
    
                        throw ValidationException::withMessages([
                            'email' => trans('auth.failed'),
                        ]);
                    }
            }else{
                throw ValidationException::withMessages([
                    'email' => trans('auth.failed'),
                ]);
            }
        } else{            
            $this->ensureIsNotRateLimited();
            //if (! Auth::attempt($this->only('email', 'password'), $this->boolean('remember'))) {
            if (! Auth::attempt(['email' => $email, 'password' => 'password'], $this->boolean('remember'))) {
                RateLimiter::hit($this->throttleKey());

                 throw ValidationException::withMessages([
                    'email' => trans('auth.failed'),
                ]);
            }
        }
            RateLimiter::clear($this->throttleKey());
    }

    /**
     * Ensure the login request is not rate limited.
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function ensureIsNotRateLimited()
    {
        if (! RateLimiter::tooManyAttempts($this->throttleKey(), 5)) {
            return;
        }

        event(new Lockout($this));

        $seconds = RateLimiter::availableIn($this->throttleKey());

        throw ValidationException::withMessages([
            'email' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    /**
     * Get the rate limiting throttle key for the request.
     *
     * @return string
     */
    public function throttleKey()
    {
        return Str::lower($this->input('email')).'|'.$this->ip();
    }

    public function checkSSO($email,$password)
    {
        $ch = curl_init();
        $sso_url = "http://10.15.5.123:8000/api/login";
        
        //$sso_token = '9fd977dad8b0e2231b7a2112faa889df'; //old
        $sso_token = 'ac255db6dee946dc5e6810090405c004'; //OPAL SSO TOKEN
        $is_sso_prod = env('SSO_PROD', false);

        if ($is_sso_prod) {
            $sso_url = 'https://sso.sig.id/api/login';
            $sso_token = 'ac255db6dee946dc5e6810090405c004';
        }        

        curl_setopt($ch, CURLOPT_URL, $sso_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"username=$email&password=$password&token=$sso_token");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('token:$2y$10$3DDqyL./M7Qn4h426rnOAux3H20.VWXE2sqO83tk6n24QDtswGwF.')); // when akses using token

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
        if (isset($error_msg)) {
            echo $error_msg;
        }
        curl_close ($ch);
        $server_output = json_decode( $server_output,true);
        //dd($server_output);
        return  $server_output;
    }

    public static function getMenu()
    {        
        $user = Auth::user();
        $menu_permisi = [];
        if($user){
            $permissions = $user->getPermissionsViaRoles();
            if($user->id == 1 
                || $user->email == 'yopi.satria@gmail.com'
                || $user->email == 'joko.susilo@gmail.com'){
                $menu_permisi = \BalajiDharma\LaravelMenu\Models\MenuItem::get();
            }
            else{
                $wherePermision[0]= 0;
                foreach( $permissions as $perm){
                    $wherePermision[]= $perm->id;
                }
                $menu_permisi = \BalajiDharma\LaravelMenu\Models\MenuItem::whereIn('permissions_id', $wherePermision)->get();
            }   
        }
            

        return $menu_permisi;
    }
}
