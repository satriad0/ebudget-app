<?php

use App\Http\Controllers\API\ApprovalController;
use App\Http\Controllers\API\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\HomeController;
use App\Http\Controllers\API\CompanyController;
use App\Http\Controllers\API\CostCenterController;
use App\Http\Controllers\API\GlAccountController;
use App\Http\Controllers\API\StatusController;
use App\Http\Controllers\API\UnitKerjaController;
use App\Http\Controllers\API\DireksiController;
use App\Http\Controllers\API\SubmissionController;
use App\Http\Controllers\API\SearchController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\UserInformationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Request JWT Token. Khusus Mobile Apps
Route::post('/request-token', [AuthController::class, 'requestToken'])
    ->name('request-jwt-token');

// Route Home
Route::get('/home', [HomeController::class, 'index']);
Route::get('/summary', [HomeController::class, 'getData']);


// Route Cost Center
Route::post('/cost-center', [CostCenterController::class, 'index']);
Route::post('/cost-center-create', [CostCenterController::class, 'store']);
Route::post('/cost-center-update', [CostCenterController::class, 'update']);
Route::post('/cost-center-delete', [CostCenterController::class, 'delete']);
Route::post('/cost-center-view', [CostCenterController::class, 'view']);
Route::post('/cost-center/users', [CostCenterController::class, 'users']);
Route::post('/cost-center/add-users', [CostCenterController::class, 'addUsers']);
Route::post('/cost-center/delete-users', [CostCenterController::class, 'deleteUsers']);
Route::post('/cost-center-search', [CostCenterController::class, 'search']);



// Route Gl Account
Route::post('/gl-account', [GlAccountController::class, 'getGlAccount']);
Route::post('/gl-account-create', [GlAccountController::class, 'create']);
Route::post('/gl-account-update', [GlAccountController::class, 'update']);
Route::post('/gl-account-delete', [GlAccountController::class, 'delete']);
Route::post('/gl-account-search', [GlAccountController::class, 'search']);

// Route Company
Route::post('/company', [CompanyController::class, 'getCompany']);
Route::post('/company-create', [CompanyController::class, 'create']);
Route::post('/company-update', [CompanyController::class, 'update']);
Route::post('/company-delete', [CompanyController::class, 'delete']);
Route::post('/company-search', [CompanyController::class, 'search']);

// Route Status
Route::post('/status', [StatusController::class, 'getStatus']);

// Route Unit Kerja
Route::post('/unit-kerja', [UnitKerjaController::class, 'getUnitKerja']);

// Route Direksi
Route::post('/direksi', [DireksiController::class, 'getDireksi']);

//Route User Information
Route::post('/getDataUser', [UserInformationController::class, 'getDataUser']);
Route::post('/getCompany', [UserInformationController::class, 'getCompany']);
Route::post('/getCostCenter', [UserInformationController::class, 'getCostCenter']);
Route::post('/syncSSO', [UserInformationController::class, 'syncSSO']);
Route::post('/syncLDAP', [UserInformationController::class, 'syncLDAP']);
// End Route user information

// Route Submission
Route::post('/{action}-submission', [SubmissionController::class, 'update'])
    ->where('action', '(save|draft-save)');
Route::post('/{action}-storesubmission', [SubmissionController::class, 'store'])
    ->where('action', '(create|draft-create)');
Route::post('/get-data', [SubmissionController::class, 'getDataByMount']);
Route::post('/get-login', [SubmissionController::class, 'getDataLogin']);
Route::post('/get-user', [SubmissionController::class, 'getAllUser']);
Route::post('/approval-atasan', [ApprovalController::class, 'index']);
Route::post('/approval-detail', [ApprovalController::class, 'view']);
Route::post('/approval-detail1', [ApprovalController::class, 'view']);
Route::post('/trans-doc', [SubmissionController::class, 'index']);
Route::post('/submission', [SubmissionController::class, 'index']);
Route::post('/submission/view', [ApprovalController::class, 'view']);
Route::post('/trans-doc-history', [SubmissionController::class, 'history']);
Route::post('/update-submission', [SubmissionController::class, 'updateSubmission']);

// search
Route::post('/user-costcenter', [SearchController::class, 'userCostCenter']);
Route::post('/costcenter', [SearchController::class, 'userCostCenterAll']);
Route::post('/list-dirbidang', [SearchController::class, 'dirBidang']);
Route::get('/search/user-costcenter', [SearchController::class, 'userCostCenter']);
Route::get('/search/list-dirbidang', [SearchController::class, 'dirBidang']);
Route::get('/search/status', [SearchController::class, 'status']);

// report Submission
Route::post('/report-submission', [SubmissionController::class, 'report']);
Route::post('/report-submissionView', [SubmissionController::class, 'reportView']);
Route::post('/approval-history', [ApprovalController::class, 'history']);

// Route Approval Mobile
Route::get('/get-approval', [SubmissionController::class, 'getDataMobile']);

Route::get('/approval', [ApprovalController::class, 'index']);
Route::get('/approval/{id}', [ApprovalController::class, 'view']);
Route::post('/approval/{id}/{action}', [ApprovalController::class, 'store'])->where('action','(approve|approvereason|revisi|reject)');


Route::group(['prefix' => 'dashboard'], function(){
    Route::get('/all', [DashboardController::class, 'all']);
    Route::get('/count/user', [DashboardController::class, 'userCount']);
    Route::get('/count/{type}-docs', [DashboardController::class, 'docCount'])
    ->where('type', '(inprogress|approve|active|reject|all)');
    Route::get('/amount/{type}-docs', [DashboardController::class, 'docAmount'])
    ->where('type', '(inprogress|approve|active|reject|all)');
});

Route::post('/user', [AuthController::class, 'getAllUser']);