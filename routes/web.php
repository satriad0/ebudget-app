<?php

use App\Http\Requests\Auth\LoginRequest;
use App\Models\Company;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::location(route('login'));
});

Route::get('/api-docs', function(){
    return view('docs', ['urlSchema' => route('api-docs.schema')]);
});
Route::get('/api-docs/schema', function(){
    $dirs = [
        app_path('Http/Controllers')
    ];
    $json = \Swagger\scan($dirs, []);
    return response()->json($json);
})->name('api-docs.schema');

// DASHBOARD
Route::get('/dashboard', function () {
    $user = Auth::user();
    $token = $user->remember_token;

    return Inertia::render('Dashboard', [
        'token' => $token,
        'nav_menu' => LoginRequest::getMenu(),
    ]);
})->middleware(['auth', 'verified'])->name('dashboard');

// HISTORY-SUBMISSION
Route::get('/unfreeze-budget', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    return Inertia::render('UnfreezeBudget/Index', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
    ]);
})->middleware(['auth', 'verified'])->name('unfreeze-budget');

Route::get('/unfreeze-budget/detail', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    // return Inertia::render('UnfreezeBudget/Detail', [
    return Inertia::render('Submission/Create', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'view' => 'Y',
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
    ]);
})->middleware(['auth', 'verified'])->name('unfreeze-budget.detail');

Route::get('/submission-view-uk', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    // return Inertia::render('UnfreezeBudget/Detail', [
    return Inertia::render('Submission/Create', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'view' => 'Y2',
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
        'p' => isset($_GET['p']) ? $_GET['p'] : "",
    ]);
})->middleware(['auth', 'verified'])->name('submission-uk.view');

Route::get('/submission-view-ap', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    // return Inertia::render('UnfreezeBudget/Detail', [
    return Inertia::render('UnfreezeBudgetAP/Create', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'view' => 'Y2',
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
        'p' => isset($_GET['p']) ? $_GET['p'] : "",
    ]);
})->middleware(['auth', 'verified'])->name('submission-ap.view');

Route::get('/submissionall', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    // return Inertia::render('UnfreezeBudget/Detail', [
    return Inertia::render('ListAllSubmission/Index', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'view' => 'Y',
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
    ]);
})->middleware(['auth', 'verified'])->name('submission.all');

Route::get('/historyapprove', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    // return Inertia::render('UnfreezeBudget/Detail', [
    return Inertia::render('HistoryApprove/Index', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'view' => 'Y',
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
    ]);
})->middleware(['auth', 'verified'])->name('submission.all');


// UNFREEZE-BUDGET-AP
Route::get('/unfreeze-budget-ap', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    return Inertia::render('UnfreezeBudgetAP/Index', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
    ]);
})->middleware(['auth', 'verified'])->name('unfreeze-budget-ap');

Route::get('/unfreeze-budget-ap-create', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    return Inertia::render('UnfreezeBudgetAP/Create', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
        'aproval' => isset($_GET['aproval']) ? $_GET['aproval'] : "",
        'p' => isset($_GET['p']) ? $_GET['p'] : ""
    ]);
})->middleware(['auth', 'verified'])->name('unfreeze-budget-ap-create');

Route::get('/unfreeze-budget-ap/detail', function () {

    $user = Auth::user();
    $token = $user->remember_token;
    $role = $user->getRoleNames()[0];

    return Inertia::render('UnfreezeBudgetAP/Create', [
        'token' => $token,
        'role' => $role,
        'nav_menu' => LoginRequest::getMenu(),
        'view' => 'Y',
        'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
        'p' => isset($_GET['p']) ? $_GET['p'] : ""
    ]);
})->middleware(['auth', 'verified'])->name('unfreeze-budget-ap.detail');

// SUBMISSION
Route::prefix('submission')->group(function () {

    Route::get('/', function () {
        return Inertia::render('Submission/Create',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
            'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
            'role' => Auth::user()->getRoleNames()[0]
        ]);
    })->name('submission');

    Route::get('/create', function () {

        return Inertia::render('Submission/Create',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
            'doc_id' => isset($_GET['id']) ? $_GET['id'] : "",
            'p' => isset($_GET['p']) ? $_GET['p'] : "",
            'aproval' => isset($_GET['aproval']) ? $_GET['aproval'] : "",
            'role' => Auth::user()->getRoleNames()[0]
        ]);
    })->name('submission.create');

})->middleware(['auth', 'verified']);

// APPROVAL
Route::get('/approval', function () {
    return Inertia::render('Approval', [
        'token' => Auth::user()->remember_token,
        'nav_menu' => LoginRequest::getMenu(),
    ]);
})->middleware(['auth', 'verified'])->name('approval');

Route::get('/approval/<id>', function ($id) {
    $user = Auth::user();
    $token = $user->remember_token;
    return Inertia::render('Approval/View', [
        'id' => $id,
        'token' => $token,
        'nav_menu' => LoginRequest::getMenu(),
    ]);
})->middleware(['auth', 'verified'])->name('approval.view');

// MASTER COST CENTER
Route::prefix('mastercost')->group(function () {
    Route::get('/', function () {
        return Inertia::render('MasterCost/Index',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastercost');
    Route::get('/edit', function () {
        return Inertia::render('MasterCost/Edit',  [
            'token' => Auth::user()->remember_token,
            'code' => Request::all(),
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastercost.edit');
    Route::get('/create', function () {
        return Inertia::render('MasterCost/Create',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastercost.create');
})->middleware(['auth', 'verified']);

// MASTER GL ACCOUNT
Route::prefix('mastergl')->group(function () {
    Route::get('/', function () {
        return Inertia::render('MasterGL/Index',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastergl');
    Route::get('/edit', function () {
        return Inertia::render('MasterGL/Edit',  [
            'token' => Auth::user()->remember_token,
            'code' => Request::all(),
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastergl.edit');
    Route::get('/create', function () {
        return Inertia::render('MasterGL/Create',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastergl.create');
})->middleware(['auth', 'verified']);

// MASTER COMPANY
Route::prefix('mastercomp')->group(function () {
    Route::get('/', function () {
        return Inertia::render('MasterComp/Index',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastercomp');
    Route::get('/edit', function () {
        return Inertia::render('MasterComp/Edit',  [
            'token' => Auth::user()->remember_token,
            'code' => Request::all(),
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastercomp.edit');
    Route::get('/create', function () {
        return Inertia::render('MasterComp/Create',  [
            'token' => Auth::user()->remember_token,
            'nav_menu' => LoginRequest::getMenu(),
        ]);
    })->name('mastercomp.create');
})->middleware(['auth', 'verified']);

require __DIR__ . '/auth.php';
